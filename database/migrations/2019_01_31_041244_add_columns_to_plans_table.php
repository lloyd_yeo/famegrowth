<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plans', function (Blueprint $table) {
            $table->string('trialDuration')->nullable();
            $table->string('trialDurationUnit')->nullable();
            $table->tinyInteger('trialPeriod')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plans', function (Blueprint $table) {
            $table->dropColumn('trialDuration');
            $table->dropColumn('trialDurationUnit');
            $table->dropColumn('trialPeriod');
        });
    }
}
