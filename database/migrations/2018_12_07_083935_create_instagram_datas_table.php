<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstagramDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instagram_datas', function (Blueprint $table) {
            $table->increments('id');
            $table->text('users')->nullable();
            $table->text('tags')->nullable();
            $table->string('instagram_username')->nullable();
            $table->string('instagram_password')->nullable();
            $table->boolean('follow')->default(false);
            $table->boolean('liking')->default(true);
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instagram_datas');
    }
}
