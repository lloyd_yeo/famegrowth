@extends('layouts.app')

@section('title')
    @include('fragments.head', [
        'title' => 'Subscribe',
    ])
@endsection

@section('side-panel')
    @include('fragments.side-panel')
@endsection

@section('content-wrapper')
    <div id="wrapper" class="clearfix">
        <style>
            .content-wrap{
                padding: 120px 0;
            }
            .main-content-block{
                margin-top: 120px;
            }
            .col_one_third{
                margin-right: 0;
            }
            #form-template .button.button-secondary{
                background-color: #eaeaea;
                color: #7a7e7f;
            }
            #form-template .button.button-success{
                background-color: #5bc57a;
            }
            #form-template .button.button-secondary, #form-template .button.button-success{
                width: 46%;
            }
            #form-template .button.button-success.full-width{
                width: auto;
            }
            #form-template .button.button-secondary:hover{
                background-color: #7d7d7d !important;
                color: #ffffff;
            }
            #form-template .button.button-success:hover{
                background-color: #3c9a57 !important;
            }
            #form-template .input-group-text{
                background: none;
                border-left: none;
                border-radius: 3px;
            }
            #form-template input.form-control, #form-template input.form-control.is-invalid{
                border-right: none;
            }
            #instagramEmail, #instagramEmailLastStep, #instagramPasswordLastStep, #instagramConfirmPasswordLastStep{
                border-right: none;
            }
            .example-block{
                border-left: 2px solid #76bbc0;
                padding: 10px;
                margin: 50px 0;
            }
            .card-body i{
                color: #5bc57a;
            }
            #form-template .card-body{
                line-height: 45px;
                font-size: 20px;
            }
            .similar-user{
                line-height: 40px;
                font-size: 20px;
                border-bottom: 1px solid #cccccc;
                position: relative;
                padding: 5px;
            }
            .similar-user input {
                border: none;
            }
            #autocomplete-block, #autocomplete-tag-block{
                position: absolute;
                top: 50px;
                z-index: 20;
                overflow-y: auto;
                max-height: 120px;
                background-color: #fff;
                border: 2px solid #eeeeee;
                border-radius: 2px;
                padding: 10px;
                width: 430px;
                -webkit-box-shadow: 0px 0px 5px 1px rgba(0,0,0,0.75);
                -moz-box-shadow: 0px 0px 5px 1px rgba(0,0,0,0.75);
                box-shadow: 0px 0px 5px 1px rgba(0,0,0,0.75);
            }
            #autocomplete-block li, #autocomplete-tag-block li{
                font-size: 16px;
                cursor: pointer;
            }
            .tag-example{
                color: #76bbc0;
                font-size: 15px;
                font-weight: 600;
                border-bottom: 2px dotted;
                margin-right: 10px;
            }
            .secondary-text, #content p.secondary-text{
                margin-bottom: 10px;
                font-size: 15px;
                font-weight: 600;
                color: #a9a2a2;
            }
            .setting-block{
                border-top: 1px solid #efefef;
                position: relative;
                padding: 20px 10px 20px 40px;
            }
            .setting-block p {
                margin-bottom: 0;
            }
            .setting-block .like-header{
                font-size: 20px;
            }
            .setting-block i{
                position: absolute;
                left: 0;
                color: #76bbc0;
            }
            .fame-switcher{
                position: absolute;
                right:0;
                top: 24px;
            }
            .custom-checkbox {
                position: relative;
                display: block;
                min-height: 1rem;
                padding-left: 0;
                margin-right: 0;
                cursor: pointer;
            }
            .custom-control-input {
                position: absolute;
                z-index: -1;
                opacity: 0;
            }
            .custom-checkbox .custom-control-indicator, .custom-checkbox .custom-control-indicator-on {
                content: "";
                display: inline-block;
                position: relative;
                width: 50px;
                height: 18px;
                background-color: #dc3545;
                border-radius: 15px;
                -webkit-transition: background .3s ease;
                transition: background .3s ease;
                vertical-align: middle;
                margin: 0 10px 0 16px;
                box-shadow: none;
            }
            .custom-checkbox .custom-control-indicator:after {
                content: "";
                position: absolute;
                display: inline-block;
                width: 20px;
                height: 20px;
                background-color: #f1f1f1;
                border-radius: 21px;
                box-shadow: 0 1px 3px 1px rgba(0, 0, 0, 0.4);
                left: -2px;
                top: -1px;
                -webkit-transition: left .3s ease, background .3s ease, box-shadow .1s ease;
                transition: left .3s ease, background .3s ease, box-shadow .1s ease;
            }
            .custom-checkbox .custom-control-input:checked ~ .custom-control-indicator {
                background-color: #5bc57a;
                background-image: none;
                box-shadow: none !important;
            }
            .custom-checkbox .custom-control-input:checked ~ .custom-control-indicator:after, .custom-checkbox .custom-control-indicator-on, .progress-bar {
                background-color: #5bc57a;
            }
            .custom-checkbox .custom-control-input:checked ~ .custom-control-indicator:after{
                left: 30px;
            }
            #follow-switcher, .custom-checkbox .custom-control-indicator-on{
                color: #fff;
            }
            .indicator-off{
                text-align: right;
                padding-right: 4px;
            }
            .indicator-on{
                text-align: left;
                padding-left: 4px;
            }
            .icon-wrapper{
                width: 80px;
            }
            .skip-link{
                font-size: 18px;
                font-weight: 500;
                color: #949494;
                cursor: pointer;
            }
            .error-input{
                border-color: #dc3545;
            }
            .progress-block{
                position: relative;
            }
            .step-wrapper, .multistep{
                display: flex;
                align-items: center;
            }
            .rouded-border {
                border: 2px solid #949494;
                border-radius: 50%;
                padding: 5px;
                width: 30px;
                height:100%;
                line-height: 15px;
                font-size: 16px;
            }
            .step-num{
                flex-basis: 30px;
                height: 30px;
            }
            .step-line, .substep-line{
                width: 100%;
                height: 2px;
                background: #949494;
            }
            .substep-line{
                min-width:5px;
            }
            .substep{
                width: 10px;
                height: 10px;
                flex-basis: 10px;
                border: 2px solid #949494;
                border-radius: 50%;
                padding:5px;
            }
            .filled-green{
                background-color: #5bc57a;
                border-color: #5bc57a;
                color: #fff;
            }
            .similar-user:hover, .similar-user:focus, .similar-user:active{
                border: 1px solid;
                border-radius: 2px;
            }
            .form-control:hover + .input-group-append .input-group-text,
            .form-control:active + .input-group-append .input-group-text,
            .form-control:focus + .input-group-append .input-group-text,
            .similar-user:hover, .similar-user:focus, .similar-user:active{
                border-color: #5bc57a;
            }
            #loader-gif{
                position: fixed;
                display: none;
                width: 100%;
                height: 100%;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
                background-color: rgba(255, 255, 255, 0.85);
                z-index: 99;
                cursor: pointer;
            }
            .loader-wrapper{

            }
            .img-wrapper{
                position: fixed;
                top: 50%;
                left: 0;
                right: 0;
                text-align: center;
            }
            .input-group:hover, .input-group:focus,
            .form-control:hover, .form-control:focus{
                border-color: #5bc57a;
            }
        </style>
        @include('fragments.navigation')
        <section id="content">

            <div class="content-wrap">
                <div class="container clearfix">
                    <div id="form-template">
                        <div class="row mb-5">
                            <div class="col-md-8 offset-2 text-center progress-block">
                                <div class="step-wrapper">
                                    <div class="step-num">
                                        <div class="rouded-border filled-green">1</div>
                                    </div>
                                    <div class="step-line" :class="{'filled-green': isStep(2)}"></div>
                                    <div class="step-num multistep">
                                        <div class="rouded-border" :class="{'filled-green': isStep(2)}">2</div>
                                        <div v-show="showSubsteps" class="multistep">
                                            <div class="substep-line" :class="{'filled-green': isStep(2)}"></div>
                                            <div class="substep" :class="{'filled-green': isStep(2)}"></div>
                                            <div class="substep-line" :class="{'filled-green': isStep(3)}"></div>
                                            <div class="substep" :class="{'filled-green': isStep(3)}"></div>
                                            <div class="substep-line" :class="{'filled-green': isStep(4)}"></div>
                                            <div class="substep" :class="{'filled-green': isStep(4)}"></div>
                                            <div class="substep-line" :class="{'filled-green': isStep(5)}"></div>
                                            <div class="substep" :class="{'filled-green': isStep(5)}"></div>
                                        </div>
                                    </div>

                                    <div class="step-line" :class="{'filled-green': isStep(6)}"></div>
                                    <div class="step-num">
                                        <div class="rouded-border" :class="{'filled-green': isStep(6)}">3</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row main-content-block">
                            <div class="col-md-3">&nbsp;</div>
                                <div class="col-md-6">
                                    <step-1 v-show="currentStepComponent == 'step-1'"></step-1>
                                    <step-2 v-show="currentStepComponent == 'step-2'"></step-2>
                                    <step-3 v-show="currentStepComponent == 'step-3'"></step-3>
                                    <step-4 v-show="currentStepComponent == 'step-4'"></step-4>
                                    <step-5 v-show="currentStepComponent == 'step-5'"></step-5>
                                    <step-6 v-show="currentStepComponent == 'step-6'"></step-6>
                                    {{--<component v-bind:is="currentStepComponent"></component>--}}
                                </div>
                            <div class="col-md-3">&nbsp;</div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div id="loader-gif">
            <div class="loader-wrapper">
                <div class="img-wrapper">
                    <img src="images/loader.svg">
                    <div><h2>Verifying</h2></div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.5.17/dist/vue.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    <script>
        Vue.component('step-1', {
            template: `<div>
                            <div class="col-md-12 mb-2 text-center">
                                <h2>Let's set up your account</h2>
                                <p>Before we start, please <b>confirm your email address</b></p>
                            </div>
                            <div>
                                <div class="col-md-12 mb-2">
                                    <div class="input-group">
                                        <input type="text" class="form-control" v-model="email" :class="{'is-invalid': error}" name="registeredEmail" id="registeredEmail" aria-describedby="inputGroupPrepend2" required>
                                        <div class="input-group-append">
                                          <span class="input-group-text form-control" :class="{'is-invalid': error}" id="inputGroupPrepend2">@</span>
                                        </div>
                                        <div class="invalid-feedback" v-text="errorMsg"></div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-2 text-center">
                                    <button @click="checkEmail" class="button button-3d button-success full-width">Procced to set up account &#8250;</button>
                                </div>
                            </div>
                       </div>`,
            data() {
                return {
                    error: false,
                    matchMsg: "Entered email doesn't match your registration email",
                    emptyMsg: "Please, enter your registered email",
                    errorMsg: this.emptyMsg,
                    email: '',
                }
            },
            methods: {
                setErrors(msg) {
                    if(typeof msg !== 'undefined'){
                        this.errorMsg = msg;
                    }

                    this.error = true;
                },
                next() {
                    return formTpl.stepUp();
                },
                checkEmail() {
                    let application = this;
                    let email = application.email;

                    if(!email.length){
                        application.setErrors(this.emptyMsg);
                        return false;
                    }

                    axios
                        .get('/check-email', {
                            params: {
                                email: email
                            }
                        })
                        .then(function(response) {
                            if(response.data.error){
                                application.setErrors(application.matchMsg);
                                return false;
                            }

                            application.next();
                        });
                }
            },
        });

        Vue.component('step-2', {
            template: `<div>
                            <div class="col-md-12 mb-2 text-center">
                                <h2>How FameGrowth works</h2>
                            </div>
                            <div class="col-md-12 mb-2">
                                <p>Using your Instagram account, <b>we find and interact with real users</b> who are likely to <br> enjoy your content and follow you back.</p>
                                <p>These interactions include:</p>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="card">
                                    <div class="card-body">
                                        <i class="i-plain icon-thumbs-up2"></i>
                                        <b>Liking their photos and videos</b>
                                    </div>
                                </div>
                                 <div class="card">
                                    <div class="card-body">
                                        <i class="i-plain icon-user"></i>
                                        <b>Following and unfollowing their accounts</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="example-block">
                                    <b>Example</b><br>
                                    If you're a luxury fashion brand, we would target users who follow Versace, Gucci,
                                    or Louis Vuitton and interact with them  by liking their photos and videos and
                                    following/unfollowing their accounts(optional).
                                </div>
                            </div>
                            <div class="col-md-12 mb-2 text-center">
                                <button @click="back" class="button button-3d button-secondary ">&#8249; Previous</button>
                                <button @click="next" class="button button-3d button-success">Next &#8250;</button>
                            </div>
                        </div>`,
            methods: {
                next() {
                    formTpl.stepUp();
                },
                back() {
                    formTpl.stepBack();
                }
            },
        });

        Vue.component('step-3', {
            template: `<div>
                             <div class="col-md-12 mb-2 text-center">
                                <h2>Enter 5 Instagram users <br> who are similar to you</h2>
                             </div>
                             <div class="col-md-12 mb-2">
                                <p><b>Example:</b> if you have a travel blog, it would make sense to target @gopro, @natgeo. <br>
                                    We'll capture the attention of their followers by liking their photos and following/unfollowing their accounts (optional)
                                </p>
                             </div>
                             <div class="col-md-12 mb-2">
                                <div class="similar-user" v-for="row in rowsStart" :id="'userrow-'+row">
                                    <input type="text" :name="'similarAccount[' + row + ']'" v-model="users[row].full_name" :data-num="row" @keyup="autocomplete" class="form-control">
                                </div>
                             </div>
                             <div class="col-md-12 mb-2 text-center">
                                <button @click="back" class="button button-3d button-secondary ">&#8249; Previous</button>
                                <button @click="saveRelated" class="button button-3d button-success">Next &#8250;</button>
                             </div>
                             <div class="col-md-12 mb-2 text-center">
                                <span @click="next" class="skip-link" >Skip this step</span>
                             </div>
                             <div v-show="show" id="autocomplete-block">
                                <ul>
                                    <li v-for="item in results" @click="chooseUser(item)">@{{ '@' + item.full_name }}, followers: @{{ item._followers_count_str }}</li>
                                </ul>
                             </div>
                       </div>`,
            data() {
                return {
                    show: false,
                    rowsStart: 5,
                    users: {
                        '1': {
                            'full_name': ''
                        },
                        '2': {
                            'full_name': ''
                        },
                        '3': {
                            'full_name': ''
                        },
                        '4': {
                            'full_name': ''
                        },
                        '5': {
                            'full_name': ''
                        },
                    },
                    results: [],
                    currentRow: 0,
                    timer: null,
                }
            },
            methods: {
                saveRelated() {
                    let application = this;
                    axios
                        .post('/storeRelatedUsers', {
                            users: application.users
                        })
                        .then(function(response) {
                            if(response.data.success){
                                application.next();
                            }
                        });
                },
                autocomplete(event){
                    let target = $(event.target);
                    let query = target.val();

                    if(query.length < 3){
                        return;
                    }

                    if (this.timer) {
                        clearTimeout(this.timer);
                        this.timer = null;
                    }
                    this.timer = setTimeout(() => {
                        this.getUsers(query, target)
                    }, 1000);
                },
                getUsers(query, target) {
                    let application = this;

                    axios
                        .get('/instagramUsers', {
                            params: {
                                search: query
                            }
                        })
                        .then(function(response) {
                            application.results = response.data;
                            application.show = true;
                            application.currentRow = target.data('num');
                            application.positionBlock();
                        });
                },
                positionBlock() {
                    $('#userrow-' + this.currentRow).append($('#autocomplete-block'));
                },
                chooseUser(item) {
                    this.users[this.currentRow] = item;
                    this.show = false;
                },
                next() {
                    formTpl.stepUp();
                },
                back() {
                    formTpl.stepBack();
                }
            },
        });

        Vue.component('step-4', {
            template: `<div>
                            <div class="col-md-12 mb-2 text-center">
                                <h2>Enter 5 hashtags<br> that  you'd like to target</h2>
                            </div>
                            <div class="col-md-12 mb-2">
                                <p><b>Example:</b> if you're a fitness  influencer, it would make sense to target users who post
                                    photos and videos using the hashtag #fitness, #fitlife or #cardio. We'll
                                    capture their attention by liking their photos and following/unfollowing their accounts(optional).
                                </p>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="card">
                                    <div class="card-body">
                                        <p class="secondary-text">SUGGESTED TAGS:</p>
                                        <p><span class="tag-example" v-for="tag in exampleTags" v-text="tag" ></span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="similar-user" v-for="row in rowsStart" :id="'tagrow-'+row">
                                    <input type="text" :name="'similarTag['+ row + ']'" :data-num="row" v-model="tags[row].name"  @keyup="autocomplete" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12 mb-2 text-center">
                                <button @click="back" class="button button-3d button-secondary ">&#8249; Previous</button>
                                <button @click="saveTags" class="button button-3d button-success">Next &#8250;</button>
                            </div>
                            <div class="col-md-12 mb-2 text-center">
                                <span @click="next" class="skip-link">Skip this step</span>
                            </div>
                            <div v-show="show" id="autocomplete-tag-block">
                                <ul>
                                    <li v-for="tag in results" @click="chooseTag(tag)">@{{ '@' + tag.name }}, followers: @{{ tag._media_count_str }}</li>
                                </ul>
                             </div>
                      </div>`,

            data() {
                return {
                    show: false,
                    rowsStart: 5,
                    tags: {
                        '1': {
                            'name': ''
                        },
                        '2': {
                            'name': ''
                        },
                        '3': {
                            'name': ''
                        },
                        '4': {
                            'name': ''
                        },
                        '5': {
                            'name': ''
                        },
                    },
                    results: [],
                    exampleTags: ["#KAD", "#ART", "#DRAWING", "#BEAUTY", "#PEOPLE", "#JAPAN", "#HAPPY"],
                    currentRow: 0,
                    timer: null,
                }
            },
            methods: {
                saveTags() {
                    let application = this;
                    axios
                        .post('/storeRelatedTags', {
                            tags: application.tags
                        })
                        .then(function(response) {
                            if(response.data.success){
                                application.next();
                            }
                        });

                },
                autocomplete(event){
                    let target = $(event.target);
                    let query = target.val();

                    if(query.length < 3){
                        return;
                    }

                    if (this.timer) {
                        clearTimeout(this.timer);
                        this.timer = null;
                    }
                    this.timer = setTimeout(() => {
                        this.getTags(query, target)
                    }, 500);
                },
                getTags(query, target) {
                    let application = this;

                    axios
                        .get('/instagramTags', {
                            params: {
                                search: query
                            }
                        })
                        .then(function(response) {
                            application.results = response.data;
                            application.show = true;
                            application.currentRow = target.data('num');
                            application.positionBlock();
                        });
                },
                positionBlock() {
                    $('#tagrow-' + this.currentRow).append($('#autocomplete-tag-block'));
                },
                chooseTag(item) {
                    this.tags[this.currentRow] = item;
                    this.show = false;
                },
                next() {
                    formTpl.stepUp();
                },
                back() {
                    formTpl.stepBack();
                }
            },

        });

        Vue.component('step-5', {
            template: `<div>
                            <div class="col-md-12 mb-2 text-center">
                                <h2>Adjust your engagement<br> preferences</h2>
                            </div>
                            <div class="col-md-12 mb-2 text-center">
                                <p>
                                    This is where you can enable or disable some of the systems we use to grow your Instagram account.
                                </p>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="setting-block">
                                    <i class="i-plain icon-user"></i> &nbsp;<b class="like-header">Follow / unfollow</b>&nbsp;&nbsp;<span class="secondary-text">RECOMMENDED</span>
                                    <div class="fame-switcher">
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" @change="switchState = !switchState">
                                            <span class="custom-control-indicator" :class="indicatorState" id="follow-switcher" v-text="switchText"></span>
                                        </label>
                                    </div>
                                    <p>Following a targeted user and then unfollowing them within 24 hours.<br>Clients who enable this feature see <b>2-3x more growth</b>.</p>
                                </div>
                                <div class="setting-block">
                                    <i class="i-plain icon-thumbs-up2"></i>&nbsp;<b class="like-header">Liking photos and videos</b>&nbsp;&nbsp;<span class="secondary-text">DEFAULT</span>
                                    <div class="fame-switcher">
                                        <label class="custom-control custom-checkbox">
                                            <span class="custom-control-indicator-on" style="text-align:center">On</span>
                                        </label>
                                    </div>
                                    <p>This is one of the primary ways we engage with targeted users. <br> You cannot turn this feature off.</p>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2 text-center">
                                <button @click="back" class="button button-3d button-secondary ">&#8249; Previous</button>
                                <button @click="saveFollow" class="button button-3d button-success">Next &#8250;</button>
                            </div>
                       </div>`,
            data() {
                return {
                    switchState: false,
                }
            },
            computed: {
                switchText: function() {
                  return this.switchState ? 'On' : 'Off';

                },
                indicatorState: function() {
                    return this.switchState ? ' indicator-on' : ' indicator-off';
                },
            },
            methods: {
                saveFollow() {
                    let application = this;
                    axios
                        .post('/instagramFollow', {
                            follow: application.switchState
                        })
                        .then(function(response) {
                            if(response.data.success){
                                application.next();
                            }
                        });
                },
                next() {
                    formTpl.stepUp();
                },
                back() {
                    formTpl.stepBack();
                }
            },
        });

        Vue.component('step-6', {
            template: `<div>
                            <div v-show="!showLastMsg">
                                <div class="col-md-12 mb-2 text-center">
                                    <div class="icon-wrapper">
                                        <img src="/images/instagram.svg">
                                    </div>
                                </div>
                                <div class="col-md-12 mb-2 text-center">
                                    <h2>Connect you Instagram</h2>
                                    <p><b>This is the last step.</b> Connect your instagram account so that your growth team can begin interacting with potential followers.</p>
                                </div>
                                <div class="col-md-12 mb-2 text-center">
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" placeholder="Instagram username" v-model="username" :class="{'is-invalid': userErrorShow}" name="instagramEmailLastStep" id="instagramEmailLastStep" aria-describedby="inputGroupPrepend2" required>
                                        <div class="input-group-append">
                                            <span class="input-group-text form-control" id="inputGroupPrepend2" :class="{'is-invalid': userErrorShow}"><img src="/images/instagram-small.svg" style="width: 20px;"></span>
                                        </div>
                                        <div class="invalid-feedback" v-text="errorUser"></div>
                                    </div>
                                    <div class="input-group mb-3">
                                        <input type="password" class="form-control" placeholder="Instagram password" v-model="password" name="instagramPasswordLastStep" :class="{'is-invalid': pswdErrorShow}" id="instagramPasswordLastStep" aria-describedby="inputGroupPrepend2" required>
                                        <div class="input-group-append">
                                            <span class="input-group-text form-control" id="inputGroupPrepend2" :class="{'is-invalid': pswdErrorShow}"><img src="/images/locked.svg" style="width: 20px;"></span>
                                        </div>
                                    </div>
                                    <div class="input-group mb-3">
                                        <input type="password" class="form-control" placeholder="Confirm password" v-model="passwordConfirm" :class="{'is-invalid': pswdErrorShow}" name="instagramConfirmPasswordLastStep" id="instagramConfirmPasswordLastStep" aria-describedby="inputGroupPrepend2" required>
                                        <div class="input-group-append">
                                            <span class="input-group-text form-control" id="inputGroupPrepend2" :class="{'is-invalid': pswdErrorShow}"><img src="/images/locked.svg" style="width: 20px;"></span>
                                        </div>
                                        <div class="invalid-feedback" v-text="errorPswd"></div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-2 text-center">
                                    <button class="button button-3d button-success" @click="connect">Connect Instagram</button>
                                </div>
                                <div class="col-md-12 mb-2">
                                    <div class="example-block">
                                        <b>Why do you need my instagram password?</b><br>
                                        We use your Instagram account strictly to like photos and videos and follow/unfollow accounts (if you enable that feature).<br>
                                        We <b>do not</b> post anything from your account or on your behalf.<br>
                                        We <b>do not</b> share your information with any third-party.<br>
                                        We <b>do not</b> use your account for anything malicious or harmful.<br>
                                        We <b>do</b> ensure that your information is kept secure in an encrypted environment - and we are <b>fully committed</b> to protecting.<br>

                                    </div>
                                </div>
                                <div class="col-md-12 mb-2 text-center">
                                    <span @click="back" class="skip-link">&#8249; Go back</span>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2 text-center" v-show="showLastMsg">
                                <img src="/images/pending.svg">
                                <h2>Connection is pending</h2>
                                <p>Thank you! You can close this page now. Our manager will contact you once your Instagram account is connected.<p>
                                <div>
                                    <p>
                                        <b>Keep in mind:</b> because we are logging  in from a different location and device, Instagram might trigger a security precaution.
                                        Don't worry, it's normal and your account is safe.
                                    </p>
                                </div>
                            </div>
                       </div>`,
            data() {
              return {
                  username: '',
                  password: '',
                  passwordConfirm: '',
                  errorUser: "Please enter your Instagram username",
                  emptyPswd: "Password cant't be blank",
                  errorConfirm: "Password and password confirm doesn't match",
                  errorPswd: this.emptyPswd,
                  userErrorShow: false,
                  pswdErrorShow: false,
                  showLastMsg: false,
              }
            },
            methods: {
                setErrors(field, msg) {
                    this[field] = true;

                    if(msg){
                        this.errorPswd = this[msg];
                    }
                },
                connect() {
                    let application = this;
                    application.userErrorShow = false;
                    application.pswdErrorShow = false;
                    if(!application.username.length){
                        application.setErrors('userErrorShow');
                        return false;
                    }

                    if(!application.checkPasswords()){
                        return false;
                    }

                    $('#loader-gif').show()

                    axios
                        .post('/instagramCredentials', {
                            username: application.username,
                            password: application.password
                        })
                        .then(function(response) {
                            $('#loader-gif').hide();
                            if(response.data.ok){
                                application.showLastMsg = true;
                            }
                        });
                },
                checkPasswords() {
                    if(!this.password.length){
                        this.setErrors('pswdErrorShow', 'emptyPswd');
                        return false;
                    }

                    if(this.password !== this.passwordConfirm){
                        this.setErrors('pswdErrorShow', 'errorConfirm');
                        return false;
                    }

                    return true;
                },
                back() {
                    formTpl.stepBack();
                }
            },
        });

        const formTpl = new Vue({
            el: '#form-template',
            data: {
                currentStep: 1,
                lastStep: 6,
                email: '',
            },
            methods: {
                stepUp() {
                    this.currentStep++;
                },
                stepBack() {
                    this.currentStep--;
                },
                isStep(num){
                    return this.currentStep >= num;
                },
            },
            computed: {
                currentStepComponent: function () {
                    return 'step-' + this.currentStep
                },
                showSubsteps: function() {
                    return this.currentStep >= 2 && this.currentStep < this.lastStep;
                }
            }
        });
    </script>

    @include('fragments.footer')
@endsection