<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="FameGrowth" />

    <!-- Stylesheets
    ============================================= -->
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700|Roboto:300,400,700" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/canvas/css/bootstrap.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/canvas/style.css') }}" type="text/css" />

    <!-- One Page Module Specific Stylesheet -->
    <link rel="stylesheet" href="{{ asset('/canvas/one-page/onepage.css') }}" type="text/css" />
    <!-- / -->

    <link rel="stylesheet" href="{{ asset('/canvas/css/dark.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/canvas/css/font-icons.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/canvas/one-page/css/et-line.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/canvas/css/animate.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/canvas/css/magnific-popup.css') }}" type="text/css" />

    <link rel="stylesheet" href="{{ asset('/canvas/one-page/css/fonts.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/canvas/one-page/css/colors.css') }}" type="text/css" />

    <link rel="stylesheet" href="{{ asset('/canvas/css/responsive.css') }}" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Document Title
    ============================================= -->
    <title>{{ $title }} | FameGrowth</title>
</head>