<!-- Header
    ============================================= -->
<header id="header" class="full-header transparent-header static-sticky" data-sticky-offset="350">

    <div id="header-wrap">

        <div class="container clearfix">

            <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

            @if (empty($dark_logo))
            <!-- Logo
            ============================================= -->
            <div id="logo">
                <a href="/welcome" class="standard-logo" data-dark-logo="{{ asset('images/famegrowth-logo-dark.png') }}"><img
                        src="{{ asset('images/famegrowth-logo.png') }}" alt="FameGrowth Logo"></a>
                <a href="/welcome" class="retina-logo" data-dark-logo="{{ asset('images/famegrowth-logo-dark.png') }}"><img
                        src="{{ asset('images/famegrowth-logo.png') }}" alt="FameGrowth Logo"></a>
            </div><!-- #logo end -->
            @else
                <!-- Logo
            ============================================= -->
                    <div id="logo">
                        <a href="/welcome" class="standard-logo" data-dark-logo="{{ asset('images/famegrowth-logo-dark.png') }}"><img
                                    src="{{ asset('images/famegrowth-logo-dark.png') }}" alt="FameGrowth Logo"></a>
                        <a href="/welcome" class="retina-logo" data-dark-logo="{{ asset('images/famegrowth-logo-dark.png') }}"><img
                                    src="{{ asset('images/famegrowth-logo-dark.png') }}" alt="FameGrowth Logo"></a>
                    </div><!-- #logo end -->
            @endif
            <!-- Primary Navigation
            ============================================= -->
            <nav id="primary-menu">

                <ul class="one-page-menu" data-easing="easeInOutExpo" data-speed="1250" data-offset="65">
                    {{--<li><a href="/pricing" data-href="#wrapper">--}}
                            {{--<div>Pricing</div>--}}
                        {{--</a></li>--}}
                    <li><a href="/pricing">
                            <div>Pricing</div>
                        </a></li>
                    <li><a href="#" data-href="#section-about" data-offset="60">
                            <div>How it Works</div>
                        </a></li>
                    <li><a href="#" data-href="#section-works">
                            <div>Reviews</div>
                        </a></li>
                    <li><a href="#" data-href="#section-services">
                            <div>FAQ</div>
                        </a></li>
                    <li>
                        <a href="/pricing">
                            <div style="font-weight: 600; font-size: 15px; letter-spacing: 0px;">GET STARTED</div>
                        </a>
                        {{--<a href="#" class="button button-border button-rounded button-blue">Get Started</a>--}}

                    </li>
                </ul>

                <div id="side-panel-trigger" class="side-panel-trigger"><a href="#"><i class="icon-reorder"></i></a>
                </div>

            </nav><!-- #primary-menu end -->

        </div>

    </div>

</header><!-- #header end -->