@extends('layouts.app')

@section('title')
    @include('fragments.head', [
        'title' => 'Subscribe',
    ])
@endsection

@section('side-panel')
    @include('fragments.side-panel')
@endsection

@section('content-wrapper')
    <style>
        #content p {
            margin-bottom: 0;
        }
        .heading-block span{
            font-size: 20px;
            text-transform: uppercase;
        }
        .list-group span{
            text-align: left;
        }
        .list-group i{
            cursor: pointer;
        }
        .btn-green{
            background-color: #1abc9c;
        }
        .checkbox-style-3-label{
            position: relative;
        }
        .checkbox-style{
            opacity: 0;
            position: absolute;
        }
        .checkbox-style, .checkbox-style-3-label{
            display: inline-block;
            vertical-align: middle;
            margin: 5px;
            cursor: pointer;
        }
        .checkbox-style-3-label:before {
            content: '';
            background: #fff;
            border: 2px solid #ddd;
            display: inline-block;
            vertical-align: middle;
            width: 24px;
            height: 24px;
            padding: 4px;
            margin-right: 10px;
            line-height: 1;
            text-align: center;
        }
        .checkbox-style:checked+.checkbox-style-3-label:before{
            content: "\e116";
            font-family: lined-icons;
            background: #5bc57a;
            color: #fff;
        }
        .inline-block{
            display: inline-block;
        }
        .radio-style, .radio-style-1-label{
            display: inline-block;
            vertical-align: middle;
            margin: 5px;
            cursor: pointer;
        }
        .radio-style{
            opacity: 0;
            position: absolute;
        }
        .radio-style-1-label{
            position: relative;
        }
        .radio-style-1-label.radio-small:before{
            border: 2px solid #ddd;
            width: 16px;
            height: 16px;
            margin: 0 8px 1px 0;
            border-radius: 50%;
            content: '';
            background: #fff;
            display: inline-block;
            vertical-align: middle;
            padding: 4px;
            margin-right: 10px;
            line-height: 1;
            text-align: center;
        }
        .radio-style:checked+.radio-style-1-label:before {
            background: #ccc;
        }
        .similar-user{
            line-height: 40px;
            font-size: 20px;
            padding: 5px;
            border-bottom: 1px solid #cccccc;
            position: relative;
        }
        .similar-user input {
            border: none;
        }
        #autocomplete-block, #autocomplete-tag-block{
            position: absolute;
            top: 50px;
            z-index: 20;
            overflow-y: auto;
            max-height: 120px;
            background-color: #fff;
            border: 2px solid #eeeeee;
            border-radius: 2px;
            padding: 10px;
            width: 430px;
            -webkit-box-shadow: 0px 0px 5px 1px rgba(0,0,0,0.75);
            -moz-box-shadow: 0px 0px 5px 1px rgba(0,0,0,0.75);
            box-shadow: 0px 0px 5px 1px rgba(0,0,0,0.75);
            text-align: left;
        }
        #autocomplete-block li, #autocomplete-tag-block li{
            font-size: 16px;
            cursor: pointer;
        }
        .modal-lg{
            max-width: 500px;
        }
        .action-icon{
            color: #dc3545;
        }
        .container.center .button.button-success{
            background-color: #5bc57a !important;
        }
        .center .target-block{
            margin: 40px 0;
        }
        .center .target-block span{
            max-width: 700px;
            margin-left: auto;
            margin-right: auto;
            display: block;
            margin-top: 10px;
            color: #403f3f;
            font-size: 23px;
            text-transform: uppercase;
            font-weight: bold;
        }
        .show-all{
            display: block;
            width: 100%;
        }
        .list-group-item:hover, .list-group-item:focus{
            border-color: #5bc57a;
        }
        .modal-footer{
            border-top: none;
            justify-content: space-around;
         }
    </style>
    <div id="wrapper" class="clearfix">
        @include('fragments.navigation')
        <section id="content" class="">
            <div id="page-title" class="content-wrap page-title-center">
                <div class="container clearfix ">
                        <h1>Dashboard</h1>
                        <p>Welcome back {{ $user->name }}</p>
                </div>
            </div>
            <div class="container center">
                <div class="target-block">
                    <span>Your targets</span>
                </div>
                <div class="row">
                    <div class="col-md-6 offset-md-3">
                        <div class="list-group">
                            <span v-for="(target, index) in targets"
                                :key="target.id"
                                v-show="index < totalShow"
                                class="list-group-item list-group-item-action clearfix">
                                @{{ itemTitle(target) }}
                                <i class="icon-remove float-right action-icon" v-show="remove" @click="deleteRow(target.id, type(target), index)"></i>
                            </span>
                        </div>
                        <div class="mb-5 mt-4" v-show="targets.length > btnShowNum">
                            <button class="button button-small button-3d nomargin show-all  button-white button-light" @click="showMore">@{{ btnTitle }}</button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 offset-md-3">
                        <button href="#" class="button button-3d button-success " data-toggle="modal" data-target=".bs-example-modal-lg">+ Add new</button>
                        <button href="#" class="button button-3d button-dark" @click="remove = !remove"><i class="icon-cogs"></i> Manage</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 offset-md-3">
                        <div class="target-block">
                            <span>Growth preferences</span>
                        </div>
                        <div class="mb-5">
                            <div class="inline-block">
                                <input id="checkbox-like" class="checkbox-style" name="user-like" type="checkbox" v-model="liking">
                                <label for="checkbox-like" class="checkbox-style-3-label">Like user posts</label>
                            </div>
                            <div class="inline-block">
                                <input id="checkbox-follow" class="checkbox-style" name="follow" type="checkbox" v-model="followed">
                                <label for="checkbox-follow" class="checkbox-style-3-label">Follow and unfollow</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-body">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4>Add new target</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </div>
                                <div class="modal-body">
                                    <div class="radio-group">
                                        <div class="inline-block">
                                            <input id="users" class="radio-style" value="users" type="radio" v-model="addType" @click="resetResults">
                                            <label for="users" class="radio-style-1-label radio-small">Users</label>
                                        </div>
                                        <div class="inline-block">
                                            <input id="tags" class="radio-style" name="radio-group" value="tags" type="radio" v-model="addType" @click="resetResults">
                                            <label for="tags" class="radio-style-1-label radio-small">Tags</label>
                                        </div>
                                    </div>
                                    <div class="" v-show="addType == 'users'">
                                        <div class="col-md-12 mb-2">
                                            <div class="similar-user" v-for="row in rowsStart" :id="'userrow-'+row">
                                                <input type="text" :name="'similarAccount[' + row + ']'" v-model="users[row].full_name" :data-num="row" @keyup="autocomplete" class="form-control">
                                            </div>
                                        </div>
                                        <div v-show="showAutocomplete && resultNotEmpty" id="autocomplete-block">
                                            <ul>
                                                <li v-for="(item, index) in results"
                                                    @click="chooseUser(item)">
                                                    @{{ '@' + item.username }}, followers: @{{ item._followers_count_str }}
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="" v-show="addType == 'tags'">
                                        <div class="col-md-12 mb-2">
                                            <div class="similar-user" v-for="row in rowsStart" :id="'tagrow-'+row">
                                                <input type="text" :name="'similarTag['+ row + ']'" :data-num="row" v-model="tags[row].name"  @keyup="autocomplete" class="form-control">
                                            </div>
                                        </div>
                                        <div v-show="showAutocomplete && resultNotEmpty" id="autocomplete-tag-block">
                                            <ul>
                                                <li v-for="(tag, index) in results" @click="chooseTag(tag)">@{{ '#' + tag.name }}, followers: @{{ tag._media_count_str }}</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button class="button button-3d button-success" @click="saveItems" data-dismiss="modal" >Save</button>
                                        <button class="button button-3d button-dark" data-dismiss="modal" @click="dismissModal">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <script src="https://cdn.jsdelivr.net/npm/vue@2.5.17/dist/vue.js"></script>
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>


        <script>
            let user_follow = {{ $user->instagram_datas->follow }};
            let user_liking = {{ $user->instagram_datas->liking }};
        </script>

        <script>
            const dashboard = new Vue({
                el: '#content',
                data() {
                    return {
                        targets: [],
                        followed: user_follow,
                        liking: user_liking,
                        itemsStart: 3,
                        totalShow: null,
                        btnShowNum: null,
                        remove: false,
                        addNew: false,
                        addType: 'users',
                        showAutocomplete: false,
                        results: [],
                        rowsStart: 5,
                        currentUserRow: 0,
                        userRows: {
                            '1': {
                                'username': ''
                            },
                            '2': {
                                'username': ''
                            },
                            '3': {
                                'username': ''
                            },
                            '4': {
                                'username': ''
                            },
                            '5': {
                                'username': ''
                            },
                        },
                        users: {},
                        currentTagRow: 0,
                        tagsRows: {
                            '1': {
                                'name': ''
                            },
                            '2': {
                                'name': ''
                            },
                            '3': {
                                'name': ''
                            },
                            '4': {
                                'name': ''
                            },
                            '5': {
                                'name': ''
                            },
                        },
                        tags: {},
                    }
                },

                created() {
                    this.getData();
                    this.resetLists();
                    this.totalShow = this.itemsStart;
                    this.btnShowNum = this.itemsStart;
                },

                watch: {
                    followed() {
                        this.follow();
                    },

                    liking() {
                        this.like();
                    }
                },

                methods: {
                    resetLists() {
                        this.users  = JSON.parse(JSON.stringify(this.userRows));
                        this.tags  = JSON.parse(JSON.stringify(this.tagsRows));
                    },

                    dismissModal() {
                        this.showAutocomplete = false;
                        this.results = [];
                        this.resetLists();
                    },

                    saveItems() {
                        axios.post('/dashboard/saveTargets', {
                                tags: this.tags,
                                users: this.users,
                            })
                            .then((response) => {
                                if(response.data.success){
                                    this.targets = response.data.list;
                                    this.dismissModal()
                                }
                            });
                    },

                    removeItems() {

                    },

                    resetResults() {
                        this.results = [];
                        this.showAutocomplete = false;
                    },

                    resultNotEmpty() {
                        return Object.keys(this.results) > 0;
                    },

                    autocomplete(event){
                        let target = $(event.target);
                        let query = target.val();

                        if(query.length < 3){
                            return;
                        }

                        if (this.timer) {
                            clearTimeout(this.timer);
                            this.timer = null;
                        }
                        this.timer = setTimeout(() => {
                            if(this.addType == 'users'){
                                this.getUsers(query, target)
                            } else {
                                this.getTags(query, target)
                            }

                        }, 1000);
                    },
                    getUsers(query, target) {
                        axios.get('/instagramUsers', {
                                params: {
                                    search: query
                                }
                            })
                            .then((response) => {
                                this.results = response.data;
                                this.showAutocomplete = true;
                                this.currentUserRow = target.data('num');
                                this.positionUserBlock();
                            });
                    },

                    positionUserBlock() {
                        $('#userrow-' + this.currentUserRow).append($('#autocomplete-block'));
                    },

                    chooseUser(item) {
                        this.users[this.currentUserRow] = item;
                        this.showAutocomplete = false;
                    },

                    getTags(query, target) {
                        let application = this;

                        axios.get('/instagramTags', {
                                params: {
                                    search: query
                                }
                            })
                            .then((response) => {
                                this.results = response.data;
                                this.showAutocomplete = true;
                                this.currentTagRow = target.data('num');
                                this.positionTagBlock();
                            });
                    },
                    positionTagBlock() {
                        $('#tagrow-' + this.currentTagRow).append($('#autocomplete-tag-block'));
                    },
                    chooseTag(item) {
                        this.tags[this.currentTagRow] = item;
                        this.showAutocomplete = false;
                    },

                    getData() {
                        axios.get('/dashboard/data')
                            .then((response) => {
                                this.targets = response.data;
                            });
                    },

                    itemTitle(item) {
                        if(typeof item.username !== 'undefined'){
                            return '@ ' + item.full_name;
                        }

                        return '# ' + item.name;
                    },

                    showMore() {
                        if(this.totalShow < this.targets.length){
                            this.totalShow = this.targets.length;
                        } else {
                            this.totalShow = this.itemsStart;
                        }
                    },

                    like() {
                        axios.post('/dashboard/liking', {
                            liking: this.liking
                        }).then((response) => {
                            console.log(response);
                        });
                    },

                    follow() {
                        axios.post('/dashboard/following', {
                            follow: this.followed
                        }).then((response) => {
                            console.log(response);
                        });
                    },

                    type(target) {
                        if(typeof target.username !== 'undefined'){
                            return 'users';
                        }

                        return 'tags';
                    },

                    deleteRow(id, type, index) {
                        this.targets.splice(index, 1);
                        axios.post('/dashboard/deleteRow', {
                            row: id,
                            type: type
                        }).then((response) => {
                            console.log(response);
                        });
                    }
                },
                computed: {
                    btnTitle() {
                        if(this.totalShow < this.targets.length) {
                            return 'Show all';
                        }

                        return 'Show less';
                    }
                },
            })
        </script>
    </div>

@endsection