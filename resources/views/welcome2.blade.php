@extends('layouts.app')

@section('title')
    @include('fragments.head', [
        'title' => 'Welcome',
    ])
@endsection

@section('side-panel')
    @include('fragments.side-panel')
@endsection

@section('content-wrapper')
    <!-- Document Wrapper
        ============================================= -->
    <div id="wrapper" class="clearfix">

    @include('fragments.navigation')

    <!-- Slider
            ============================================= -->
        <section id="slider" class="slider-element slider-parallax full-screen">

            <div class="slider-parallax-inner">

                <div class="full-screen section nopadding nomargin noborder ohidden"
                     style="background: #FFF url('{{ asset('canvas/one-page/images/page/iphone-slider4.jpg') }}'); background-size: cover; background-position: center center;">
                    <div class="container clearfix">

                        <div class="vertical-middle tleft ignore-header">
                            <div class="emphasis-title bottommargin-sm">
                                <h1 class="uppercase t600"
                                    style="font-size: 56px; letter-spacing: 8px; color:white; text-shadow: 3px 3px #000000;">
                                    EXPLOSIVE INSTAGRAM GROWTH<br>
                                    BECOME A ROCKSTAR</h1>
                                <p class="lead topmargin-sm nobottommargin"
                                   style="font-size: 30px; max-width:700px; line-height: 1.7; color:white; text-shadow: 2px 2px #000000;">
                                    Real Growth, Real Fans, No Gimmicks.
                            </div>
                            <a href="{{ route('pricing') }}" class="button button-circle button-large t300 noleftmargin"
                               data-scrollto="#section-works" data-easing="easeInOutExpo" data-speed="1250"
                               data-offset="70">
                                <i class="icon-googleplay"></i>Get Started</a>
                        </div>

                    </div>
                </div>

                <a href="#" data-scrollto="#section-about" data-easing="easeInOutExpo" data-speed="1250"
                   data-offset="70"
                   class="one-page-arrow text-white" style="text-shadow: 2px 2px #000000;"><i
                            class="icon-angle-down infinite animated fadeInDown"></i></a>

            </div>

        </section><!-- #slider end -->

        <!-- Content
        ============================================= -->
        <section id="content">

            <div class="content-wrap nopadding">

                <div id="section-about" class="center page-section">

                    <div class="container clearfix">

                        <h2 class="divcenter bottommargin font-body" style="max-width: 700px; font-size: 40px;">Real
                            Fans.
                            Real People. Real Growth.</h2>

                        <div class="col_three_fifth">
                            <p class="lead divcenter bottommargin" style="max-width: 800px;">
                                Don’t waste your money on other services that offer fake likes and fake followers who
                                don’t
                                interact with your account at all.

                                With our genuine marketing techniques, your Instagram account will get the exposure it
                                needs
                                to generate <b>organic, active followers.</b>
                            </p>

                        </div>
                        <div class="col_two_fifth col_last">
                            <img class="divcenter bottommargin" style="max-height:200px; border-radius: 20px;"
                                 src="{{ asset('canvas/one-page/images/page/instagram-growth.png') }}"
                                 alt="Instagram Growth">
                        </div>

                        <p class="bottommargin" style="font-size: 16px;"><a href="{{ route('pricing') }}" data-scrollto="#section-targeting"
                                                                            data-easing="easeInOutExpo"
                                                                            data-speed="1250"
                                                                            data-offset="70" class="more-link">
                                Learn more <i class="icon-angle-right"></i></a></p>
                        <div class="clear"></div>
                        <div class="row topmargin-lg divcenter" style="max-width: 1000px; display:none;">

                            <div class="col-md-4 bottommargin">

                                <div class="team">
                                    <div class="team-image">
                                        <img src="{{ asset('canvas/one-page/images/team/1.jpg') }}" alt="John Doe">
                                        <div class="team-overlay">
                                            <div class="team-social-icons">
                                                <a href="#" class="social-icon si-borderless si-small si-facebook"
                                                   title="Facebook">
                                                    <i class="icon-facebook"></i>
                                                    <i class="icon-facebook"></i>
                                                </a>
                                                <a href="#" class="social-icon si-borderless si-small si-twitter"
                                                   title="Twitter">
                                                    <i class="icon-twitter"></i>
                                                    <i class="icon-twitter"></i>
                                                </a>
                                                <a href="#" class="social-icon si-borderless si-small si-github"
                                                   title="Github">
                                                    <i class="icon-github"></i>
                                                    <i class="icon-github"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="team-desc team-desc-bg">
                                        <div class="team-title"><h4>John Doe</h4><span>CEO</span></div>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-4 bottommargin">

                                <div class="team">
                                    <div class="team-image">
                                        <img src="{{ asset('canvas/one-page/images/team/2.jpg') }}" alt="Josh Clark">
                                        <div class="team-overlay">
                                            <div class="team-social-icons">
                                                <a href="#" class="social-icon si-borderless si-small si-twitter"
                                                   title="Twitter">
                                                    <i class="icon-twitter"></i>
                                                    <i class="icon-twitter"></i>
                                                </a>
                                                <a href="#" class="social-icon si-borderless si-small si-linkedin"
                                                   title="LinkedIn">
                                                    <i class="icon-linkedin"></i>
                                                    <i class="icon-linkedin"></i>
                                                </a>
                                                <a href="#" class="social-icon si-borderless si-small si-flickr"
                                                   title="Flickr">
                                                    <i class="icon-flickr"></i>
                                                    <i class="icon-flickr"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="team-desc team-desc-bg">
                                        <div class="team-title"><h4>Mary Jane</h4><span>Co-Founder</span></div>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-4 bottommargin">

                                <div class="team">
                                    <div class="team-image">
                                        <img src="{{ asset('canvas/one-page/images/team/3.jpg') }}" alt="Mary Jane">
                                        <div class="team-overlay">
                                            <div class="team-social-icons">
                                                <a href="#" class="social-icon si-borderless si-small si-twitter"
                                                   title="Twitter">
                                                    <i class="icon-twitter"></i>
                                                    <i class="icon-twitter"></i>
                                                </a>
                                                <a href="#" class="social-icon si-borderless si-small si-vimeo"
                                                   title="Vimeo">
                                                    <i class="icon-vimeo"></i>
                                                    <i class="icon-vimeo"></i>
                                                </a>
                                                <a href="#" class="social-icon si-borderless si-small si-instagram"
                                                   title="Instagram">
                                                    <i class="icon-instagram"></i>
                                                    <i class="icon-instagram"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="team-desc team-desc-bg">
                                        <div class="team-title"><h4>Josh Clark</h4><span>Sales</span></div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>

                <div id="section-targeting" class="page-section notoppadding nobottompadding">
                    <div class="section nomargin">
                        <div class="container clearfix">
                            <div class="divcenter center" style="max-width: 900px;">

                                <h2 class="divcenter center uppercase t300 ls3 font-body">Target your potential
                                    fans</h2>

                                <p class="lead divcenter" style="max-width: 800px;">
                                    Instagram is a diverse community with over 800 million users – with our service, you
                                    can
                                    target any type of user from any industry.
                                    <br/>
                                    Whether you’re an individual, a business, or an influencer, our targeted growth will
                                    help you <b>get Instagram famous</b>.
                                </p>
                            </div>
                        </div>
                    </div>

                    <!-- Portfolio Items
                    ============================================= -->
                    <div id="portfolio"
                         class="portfolio grid-container portfolio-nomargin portfolio-full portfolio-masonry mixed-masonry clearfix">

                        <article class="portfolio-item pf-media pf-icons wide">
                            <div class="portfolio-image">
                                <a href="#">
                                    <img src="{{ asset('canvas/one-page/images/page/travel.png') }}" alt="Travel Niche">
                                </a>
                                <div class="portfolio-overlay">
                                    <div class="portfolio-desc">
                                        <h3><a href="#">Travel</a></h3>
                                        <span><a href="#">Niche</a></span>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="portfolio-item pf-illustrations">
                            <div class="portfolio-image">
                                <a href="#">
                                    <img src="{{ asset('canvas/one-page/images/page/ecommerce.png') }}"
                                         alt="Ecommerce Niche">
                                </a>
                                <div class="portfolio-overlay">
                                    <div class="portfolio-desc">
                                        <h3><a href="#">E-Commerce</a></h3>
                                        <span><a href="#">Niche</a></span>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="portfolio-item pf-graphics pf-uielements">
                            <div class="portfolio-image">
                                <a href="#">
                                    <img src="{{ asset('canvas/one-page/images/page/business.png') }}"
                                         alt="Business Niche">
                                </a>
                                <div class="portfolio-overlay">
                                    <div class="portfolio-desc">
                                        <h3><a href="#">Business</a></h3>
                                        <span><a href="#">Niche</a></span>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="portfolio-item pf-graphics pf-uielements">
                            <div class="portfolio-image">
                                <a href="#">
                                    <img src="{{ asset('canvas/one-page/images/page/lifestyle.png') }}"
                                         alt="Lifestyle Niche">
                                    {{--<img src="{{ asset('canvas/one-page/images/page/luxury-trimmed.png') }}"--}}
                                    {{--alt="Lifestyle Niche">--}}
                                </a>
                                <div class="portfolio-overlay">
                                    <div class="portfolio-desc">
                                        <h3><a href="#">Lifestyle</a></h3>
                                        <span><a href="#">Niche</a></span>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="portfolio-item pf-graphics pf-uielements">
                            <div class="portfolio-image">
                                <a href="#">
                                    <img src="{{ asset('canvas/one-page/images/page/fitness.png') }}"
                                         alt="Fitness Niche">
                                </a>
                                <div class="portfolio-overlay">
                                    <div class="portfolio-desc">
                                        <h3><a href="#">Fitness</a></h3>
                                        <span><a href="#">Niche</a></span>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="portfolio-item pf-graphics pf-uielements">
                            <div class="portfolio-image">
                                <a href="#">
                                    <img src="{{ asset('canvas/one-page/images/page/photography.png') }}"
                                         alt="Photography Niche">
                                </a>
                                <div class="portfolio-overlay">
                                    <div class="portfolio-desc">
                                        <h3><a href="#">Photography</a></h3>
                                        <span><a href="#">Niche</a></span>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="portfolio-item pf-graphics pf-uielements">
                            <div class="portfolio-image">
                                <a href="#">
                                    <img src="{{ asset('canvas/one-page/images/page/fashion.png') }}"
                                         alt="Fashion Niche">
                                </a>
                                <div class="portfolio-overlay">
                                    <div class="portfolio-desc">
                                        <h3><a href="#">Fashion</a></h3>
                                        <span><a href="#">Niche</a></span>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="portfolio-item pf-graphics pf-uielements">
                            <div class="portfolio-image">
                                <a href="#">
                                    <img src="{{ asset('canvas/one-page/images/page/musician.png') }}"
                                         alt="Musician Niche">
                                </a>
                                <div class="portfolio-overlay">
                                    <div class="portfolio-desc">
                                        <h3><a href="#">Music</a></h3>
                                        <span><a href="#">Niche</a></span>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="portfolio-item pf-graphics pf-uielements">
                            <div class="portfolio-image">
                                <a href="#">
                                    <img src="{{ asset('canvas/one-page/images/page/cosmetics.png') }}"
                                         alt="Beauty Niche">
                                </a>
                                <div class="portfolio-overlay">
                                    <div class="portfolio-desc">
                                        <h3><a href="#">Beauty</a></h3>
                                        <span><a href="#">Niche</a></span>
                                    </div>
                                </div>
                            </div>
                        </article>

                    </div><!-- #portfolio end -->

                    {{--<div class="topmargin center"><a href="#" class="button button-border button-circle t600">--}}
                    {{--Get Started Now--}}
                    {{--</a></div>--}}

                </div>

                <div class="page-section parallax parallax-bg nomargin notopborder" data-rellax-speed="2"
                     style="background: url('{{ asset('canvas/images/parallax/3.jpg') }}') center center; padding: 100px 0;"
                     data-bottom-top="background-position:0px 0px;"
                     data-top-bottom="background-position:0px -200px;">
                    <div class="container-fluid center clearfix">

                        <div class="heading-block">
                            <h2>Set it and forget it.</h2>
                            <div class="clear"></div>
                        </div>

                        <div>
                            <p class="lead divcenter bottommargin" style="max-width: 800px; text-align:left;">
                                Setting up your campaign is easy – that means no setup fees, no downloads and no
                                complicated
                                settings.
                                <br/>
                                Then it’s hands off. <b>Your growth strategist does all the hard work for you</b> –
                                updating
                                you with your results along the way.
                            </p>
                        </div>
                    </div>
                </div>

                <div id="section-services" class="page-section notoppadding">

                    <div class="section" style="background-color:#FFFFFF;">
                        <div class="container clearfix">
                            <div class="divcenter center" style="max-width: 900px;">
                                <h2 class="divcenter bottommargin font-body" style="max-width: 900px; font-size: 45px;">
                                    Grow
                                    your Instagram Account now!</h2>

                                <a href="{{ route('pricing') }}"
                                   style="font-size: 18px; max-width: 200px;"
                                   class="divcenter button button-border button-circle button-rounded button-blue button-full notopmargin nobottommargin">GET
                                    STARTED</a>
                            </div>
                        </div>
                    </div>

                    <div class="section dark nomargin" style="display:none;">
                        <div class="divcenter center" style="max-width: 900px;">
                            <h2 class="nobottommargin t300 ls1"><b>Grow your instagram account now</b>
                                <a href="#"
                                   data-scrollto="#template-contactform"
                                   data-offset="140"
                                   data-easing="easeInOutExpo"
                                   data-speed="1250"
                                   class="button button-border button-circle button-light button-fill notopmargin nobottommargin"
                                   style="position: relative; top: -3px; font-size: 30px;">Get
                                    Started</a></h2>
                        </div>
                    </div>

                    <div class="section parallax nomargin dark"
                         style="background-image: url('{{ asset('canvas/one-page/images/page/slider-2.jpg') }}'); padding: 150px 0;"
                         data-bottom-top="background-position:0px 0px;"
                         data-top-bottom="background-position:0px -300px;">

                        <div class="container center clearfix">

                            <div class="heading-block">
                                <h2 class="divcenter center">Hear from our clients</h2>
                                <div class="clear"></div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6 bottommargin">
                                    <div class="testimonial">
                                        <div class="testi-image">
                                            <a href="#"><img src="{{ asset('canvas/images/testimonials/1.jpg') }}"
                                                             alt="Customer Testimonails"></a>
                                        </div>
                                        <div class="testi-content">
                                            <p>Incidunt deleniti blanditiis quas aperiam recusandae consequatur ullam
                                                quibusdam cum libero illo rerum repellendus!</p>
                                            <div class="testi-meta">
                                                John Doe
                                                <span>XYZ Inc.</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 bottommargin">
                                    <div class="testimonial">
                                        <div class="testi-image">
                                            <a href="#"><img src="{{ asset('canvas/images/testimonials/3.jpg') }}"
                                                             alt="Customer Testimonails"></a>
                                        </div>
                                        <div class="testi-content">
                                            <p>Natus voluptatum enim quod necessitatibus quis expedita harum provident
                                                eos
                                                obcaecati id culpa corporis molestias.</p>
                                            <div class="testi-meta">
                                                Collis Ta'eed
                                                <span>Envato Inc.</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="section-contact" class="page-section notoppadding">

                    <div class="container center clearfix">

                        <div class="heading-block">
                            <h2 class="divcenter center">Have more than 10 accounts? Contact us!</h2>
                            <div class="clear"></div>
                        </div>

                        <div class="divcenter topmargin" style="max-width: 850px;">

                            <div class="contact-widget">

                                <div class="contact-form-result"></div>

                                <form class="nobottommargin" id="template-contactform" name="template-contactform"
                                      action="include/sendemail.php" method="post">

                                    <div class="form-process"></div>

                                    <div class="col_half">
                                        <input type="text" id="template-contactform-name"
                                               name="template-contactform-name"
                                               value="" class="sm-form-control border-form-control required"
                                               placeholder="Name"/>
                                    </div>
                                    <div class="col_half col_last">
                                        <input type="email" id="template-contactform-email"
                                               name="template-contactform-email" value=""
                                               class="required email sm-form-control border-form-control"
                                               placeholder="Email Address"/>
                                    </div>

                                    <div class="clear"></div>

                                    <div class="col_one_third">
                                        <input type="text" id="template-contactform-phone"
                                               name="template-contactform-phone"
                                               value="" class="sm-form-control border-form-control"
                                               placeholder="Phone"/>
                                    </div>

                                    <div class="col_two_third col_last">
                                        <input type="text" id="template-contactform-subject"
                                               name="template-contactform-subject" value=""
                                               class="required sm-form-control border-form-control"
                                               placeholder="Subject"/>
                                    </div>

                                    <div class="clear"></div>

                                    <div class="col_full">
                                            <textarea class="required sm-form-control border-form-control"
                                                      id="template-contactform-message"
                                                      name="template-contactform-message"
                                                      rows="7" cols="30" placeholder="Your Message"></textarea>
                                    </div>

                                    <div class="col_full center">
                                        <button class="button button-border button-circle t500 noleftmargin topmargin-sm"
                                                type="submit" id="template-contactform-submit"
                                                name="template-contactform-submit" value="submit">Send Message
                                        </button>
                                        <br>
                                        <small style="display: block; font-size: 13px; margin-top: 15px;">We'll do our
                                            best
                                            to get back to you within 48 hours.
                                        </small>
                                    </div>

                                    <div class="clear"></div>

                                    <div class="col_full hidden">
                                        <input type="text" id="template-contactform-botcheck"
                                               name="template-contactform-botcheck" value="" class="sm-form-control"/>
                                    </div>

                                </form>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </section><!-- #content end -->

        @include('fragments.footer')

    </div><!-- #wrapper end -->
@endsection