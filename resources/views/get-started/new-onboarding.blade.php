@extends('layouts.app')

@section('title')
    @include('fragments.head', [
        'title' => 'Pricing',
    ])
@endsection

@section('side-panel')
    @include('fragments.side-panel')
@endsection

@section('content-wrapper')
    <!-- Document Wrapper
        ============================================= -->
    <div id="wrapper" class="clearfix">

    @include('fragments.navigation', [ 'dark_logo' => true ])

    <!-- Slider
            ============================================= -->
        <section id="slider" class="slider-element slider-parallax full-screen">

            <div class="slider-parallax-inner">

                <div class="full-screen section nopadding nomargin noborder ohidden"
                     style="background: #FFF url('{{ asset('canvas/images/parallax/3.jpg') }}'); background-size: cover; background-position: center center;">
                     {{--style="background: #000;">--}}
                    <div class="container clearfix">

                        {{--<div class="center clearfix vertical-middle tleft ignore-header">--}}
                            <div class="center clearfix tleft ignore-header vertical-middle" style="padding-top: 100px;">
                            <h1>LET'S GET STARTED!</h1>
                            <div class="col_one_fourth" style="height: 1px;"></div>
                            <div class="center card nobottommargin col_half col_last">

                                <div class="card-body center" style="padding: 40px;">
                                    <form id="login-form" name="login-form" class="nobottommargin" action="/member/get-started" method="post">

                                        <p style="font-family: 'Roboto', sans-serif; font-size: 16px;">
                                            Please enter your email address and answer a few questions so we can assist you on your Instagram growth goals.
                                        </p>
                                        <div class="col_full">
                                            <label for="login-form-username">Email Address:</label>
                                            <input type="text" style="text-align:center;" id="login-form-username"
                                                   name="email" value="" class="sm-form-control" />
                                        </div>

                                        <div class="col_full">
                                            <label for="login-form-password">Instagram Username:</label>
                                            <input type="text" style="text-align:center;" id="login-form-password"
                                                   name="instagram-username"
                                                   value="" class="sm-form-control" />
                                        </div>

                                        <div class="col_full nobottommargin">
                                            <button class="button button-3d button-dirtygreen nomargin" id="login-form-submit"
                                                    name="login-form-submit" value="next">Next</button>
                                            {{--<a href="#" class="fright">Forgot Password?</a>--}}
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <a href="#" data-scrollto="#section-what-you-get" data-easing="easeInOutExpo" data-speed="1250"
                   data-offset="70"
                   class="one-page-arrow text-white" style="text-shadow: 2px 2px #000000;"><i
                            class="icon-angle-down infinite animated fadeInDown"></i></a>

            </div>

        </section><!-- #slider end -->

        <!-- Content
        ============================================= -->
        <section id="content">

            <div class="content-wrap nopadding">
                <div id="section-what-you-get" class="page-section notoppadding nobottompadding">
                    <div class="section nomargin">
                        <div class="container clearfix">
                            <div class="divcenter center" style="max-width: 900px;">

                                <h2 class="divcenter center uppercase t300 ls3 font-body">What you get</h2>

                                <p class="lead divcenter" style="max-width: 800px;">
                                    Every subscription includes the following features:
                                </p>
                            </div>
                            <div class="col_half">
                                <div class="feature-box fbox-center fbox-plain fadeIn animated" data-animate="fadeIn">
                                    <div class="fbox-icon">
                                        <a href="#"><i class="icon-bolt"></i></a>
                                    </div>
                                    <h3>Real Instagram Growth</h3>
                                </div>
                            </div>
                            <div class="col_half col_last">
                                <div class="feature-box fbox-center fbox-plain fadeIn animated" data-animate="fadeIn">
                                    <div class="fbox-icon">
                                        <a href="#"><i class="icon-line-check"></i></a>
                                    </div>
                                    <h3>Tailored Strategy</h3>
                                </div>
                            </div>
                            <div class="col_half">
                                <div class="feature-box fbox-center fbox-plain fadeIn animated" data-animate="fadeIn">
                                    <div class="fbox-icon">
                                        <a href="#"><i class="icon-bullseye"></i></a>
                                    </div>
                                    <h3>Targeted Audience</h3>
                                </div>
                            </div>
                            <div class="col_half col_last">
                                <div class="feature-box fbox-center fbox-plain fadeIn animated" data-animate="fadeIn">
                                    <div class="fbox-icon">
                                        <a href="#"><i class="icon-line-cloud"></i></a>
                                    </div>
                                    <h3>Safe & Secure</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="section-services" class="page-section notoppadding nobottompadding">

                    <div class="section" style="background-color:#FFFFFF;">
                        <div class="container clearfix">
                            <div class="divcenter center" style="max-width: 900px;">
                                <h2 class="divcenter bottommargin font-body" style="max-width: 900px; font-size: 45px;">Popular Questions</h2>

                                {{--<a href="#"--}}
                                {{--style="font-size: 18px; max-width: 200px;"--}}
                                {{--class="divcenter button button-border button-circle button-rounded button-blue button-full notopmargin nobottommargin">GET--}}
                                {{--STARTED</a>--}}

                                <div class="row topmargin-sm clearfix">
                                    <div class="col-lg-6 bottommargin">
                                        {{--<i class="i-plain color i-large icon-line2-screen-desktop inline-block" style="margin-bottom: 15px;"></i>--}}
                                        <div class="heading-block nobottomborder" style="margin-bottom: 15px;">
                                            {{--<span class="before-heading">Scalable on Devices.</span>--}}
                                            <h4>How does it work?</h4>
                                        </div>
                                        <p>Unlike our competitors, we don’t sell you fake followers. We’ve spent years developing specialized marketing techniques to help you see real, targeted results. Our service is hands free and requires absolutely no work from your end. Just connect your account and watch your account grow.</p>
                                    </div>

                                    <div class="col-lg-6 bottommargin">
                                        {{--<i class="i-plain color i-large icon-line2-energy inline-block" style="margin-bottom: 15px;"></i>--}}
                                        <div class="heading-block nobottomborder" style="margin-bottom: 15px;">
                                            {{--<span class="before-heading">Smartly Coded &amp; Maintained.</span>--}}
                                            <h4>What payment methods are accepted?</h4>
                                        </div>
                                        <p>We accept <b>all major credit cards</b>, including Visa, American Express, Discover, Mastercard, JBC, and Diner's Club.</p>
                                    </div>

                                    <div class="col-lg-6 bottommargin">
                                        {{--<i class="i-plain color i-large icon-line2-equalizer inline-block" style="margin-bottom: 15px;"></i>--}}
                                        <div class="heading-block nobottomborder" style="margin-bottom: 15px;">
                                            {{--<span class="before-heading">Flexible &amp; Customizable.</span>--}}
                                            <h4>What happens when the plan ends?</h4>
                                        </div>
                                        <p>Nothing! Our system keeps your account in good standing so you don't need to worry about renewing anything!</p>
                                    </div>

                                    <div class="col-lg-6 bottommargin">
                                        {{--<i class="i-plain color i-large icon-line2-equalizer inline-block" style="margin-bottom: 15px;"></i>--}}
                                        <div class="heading-block nobottomborder" style="margin-bottom: 15px;">
                                            {{--<span class="before-heading">Flexible &amp; Customizable.</span>--}}
                                            <h4>Is it for me?</h4>
                                        </div>
                                        <p>Instagram is the fastest growing social media platform with over 700 million users. This means that we can reach any target audience regardless of your industry. Whether you’re a small local business or a large online brand, we’ve got you covered.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </section><!-- #content end -->

        @include('fragments.footer')

    </div><!-- #wrapper end -->
@endsection