@extends('layouts.app')

@section('title')
    @include('fragments.head', [
        'title' => 'Pricing',
    ])
@endsection

@section('side-panel')
    @include('fragments.side-panel')
@endsection

@section('content-wrapper')
    <!-- Document Wrapper
        ============================================= -->
    <div id="wrapper" class="clearfix">

    @include('fragments.navigation')

    <!-- Slider
            ============================================= -->
        <section id="slider" class="slider-element slider-parallax full-screen">

            <div class="slider-parallax-inner">

                <div class="full-screen section nopadding nomargin noborder ohidden"
                     style="background: #FFF url('{{ asset('canvas/images/parallax/3.jpg') }}'); background-size: cover; background-position: center center;">
                    <div class="container clearfix">

                        <div class="center clearfix vertical-middle tleft ignore-header">
                            <div class="heading-block" style="margin-top:30px;">
                                {{--<h2>REAL, TARGETED GROWTH.<br/>GET STARTED TODAY.</h2>--}}
                                <h2 class="font-body divcenter" style="max-width: 700px; font-size: 50px; line-height: 48px; font-weight: 800;">
                                    {{--REAL, TARGETED GROWTH.<br/>--}}
                                    GET STARTED TODAY.</h2>
                                <div class="clear"></div>
                            </div>

                            <div class="row pricing bottommargin clearfix">

                                <div class="col-md-4">

                                    <div class="pricing-box">
                                        <div class="pricing-title">
                                            <h3>Starter</h3>
                                        </div>
                                        <div class="pricing-price">
                                            <span class="price-unit">&dollar;</span>39<span class="price-tenure">/mo</span>
                                        </div>
                                        <div class="pricing-features">
                                            <ul>
                                                <li><i class="icon-user"></i><strong>1</strong> Instagram Profile</li>
                                                <li><i class="icon-code"></i> Source Files</li>
                                                <li><strong>100</strong> User Accounts</li>
                                                <li><strong>1 Year</strong> License</li>
                                                <li>Phone &amp; Email Support</li>
                                            </ul>
                                        </div>
                                        <div class="pricing-action">
                                            <a href="#" class="btn btn-danger btn-block btn-lg">Sign Up</a>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-4">

                                    <div class="pricing-box best-price">
                                        <div class="pricing-title">
                                            <h3>Premium</h3>
                                            <span>Most Popular</span>
                                        </div>
                                        <div class="pricing-price">
                                            <span class="price-unit">&dollar;</span>99<span class="price-tenure">/mo</span>
                                        </div>
                                        <div class="pricing-features">
                                            <ul>
                                                <li><strong>Full</strong> Access</li>
                                                <li><i class="icon-code"></i> Source Files</li>
                                                <li><strong>1000</strong> User Accounts</li>
                                                <li><strong>2 Years</strong> License</li>
                                                <li><i class="icon-star3"></i>
                                                    <i class="icon-star3"></i>
                                                    <i class="icon-star3"></i>
                                                    <i class="icon-star3"></i>
                                                    <i class="icon-star3"></i></li>
                                            </ul>
                                        </div>
                                        <div class="pricing-action">
                                            <a href="#" class="btn btn-danger btn-block btn-lg bgcolor border-color">Sign Up</a>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-4">

                                    <div class="pricing-box">
                                        <div class="pricing-title">
                                            <h3>Business</h3>
                                        </div>
                                        <div class="pricing-price">
                                            <span class="price-unit">&dollar;</span>19<span class="price-tenure">/mo</span>
                                        </div>
                                        <div class="pricing-features">
                                            <ul>
                                                <li><strong>Full</strong> Access</li>
                                                <li><i class="icon-code"></i> Source Files</li>
                                                <li><strong>500</strong> User Accounts</li>
                                                <li><strong>3 Years</strong> License</li>
                                                <li>Phone &amp; Email Support</li>
                                            </ul>
                                        </div>
                                        <div class="pricing-action">
                                            <a href="#" class="btn btn-danger btn-block btn-lg">Sign Up</a>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <a href="#" data-scrollto="#section-what-you-get" data-easing="easeInOutExpo" data-speed="1250"
                   data-offset="70"
                   class="one-page-arrow text-white" style="text-shadow: 2px 2px #000000;"><i
                            class="icon-angle-down infinite animated fadeInDown"></i></a>

            </div>

        </section><!-- #slider end -->

        <!-- Content
        ============================================= -->
        <section id="content">

            <div class="content-wrap nopadding">
                <div id="section-what-you-get" class="page-section notoppadding nobottompadding">
                    <div class="section nomargin">
                        <div class="container clearfix">
                            <div class="divcenter center" style="max-width: 900px;">

                                <h2 class="divcenter center uppercase t300 ls3 font-body">What you get</h2>

                                <p class="lead divcenter" style="max-width: 800px;">
                                    Every subscription includes the following features:
                                </p>
                            </div>
                            <div class="col_half">
                                <div class="feature-box fbox-center fbox-plain fadeIn animated" data-animate="fadeIn">
                                    <div class="fbox-icon">
                                        <a href="#"><i class="icon-bolt"></i></a>
                                    </div>
                                    <h3>Real Instagram Growth</h3>
                                </div>
                            </div>
                            <div class="col_half col_last">
                                <div class="feature-box fbox-center fbox-plain fadeIn animated" data-animate="fadeIn">
                                    <div class="fbox-icon">
                                        <a href="#"><i class="icon-line-check"></i></a>
                                    </div>
                                    <h3>Tailored Strategy</h3>
                                </div>
                            </div>
                            <div class="col_half">
                                <div class="feature-box fbox-center fbox-plain fadeIn animated" data-animate="fadeIn">
                                    <div class="fbox-icon">
                                        <a href="#"><i class="icon-bullseye"></i></a>
                                    </div>
                                    <h3>Targeted Audience</h3>
                                </div>
                            </div>
                            <div class="col_half col_last">
                                <div class="feature-box fbox-center fbox-plain fadeIn animated" data-animate="fadeIn">
                                    <div class="fbox-icon">
                                        <a href="#"><i class="icon-line-cloud"></i></a>
                                    </div>
                                    <h3>Safe & Secure</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="section-services" class="page-section notoppadding nobottompadding">

                    <div class="section" style="background-color:#FFFFFF;">
                        <div class="container clearfix">
                            <div class="divcenter center" style="max-width: 900px;">
                                <h2 class="divcenter bottommargin font-body" style="max-width: 900px; font-size: 45px;">Popular Questions</h2>

                                {{--<a href="#"--}}
                                {{--style="font-size: 18px; max-width: 200px;"--}}
                                {{--class="divcenter button button-border button-circle button-rounded button-blue button-full notopmargin nobottommargin">GET--}}
                                {{--STARTED</a>--}}

                                <div class="row topmargin-sm clearfix">
                                    <div class="col-lg-6 bottommargin">
                                        {{--<i class="i-plain color i-large icon-line2-screen-desktop inline-block" style="margin-bottom: 15px;"></i>--}}
                                        <div class="heading-block nobottomborder" style="margin-bottom: 15px;">
                                            {{--<span class="before-heading">Scalable on Devices.</span>--}}
                                            <h4>How does it work?</h4>
                                        </div>
                                        <p>Unlike our competitors, we don’t sell you fake followers. We’ve spent years developing specialized marketing techniques to help you see real, targeted results. Our service is hands free and requires absolutely no work from your end. Just connect your account and watch your account grow.</p>
                                    </div>

                                    <div class="col-lg-6 bottommargin">
                                        {{--<i class="i-plain color i-large icon-line2-energy inline-block" style="margin-bottom: 15px;"></i>--}}
                                        <div class="heading-block nobottomborder" style="margin-bottom: 15px;">
                                            {{--<span class="before-heading">Smartly Coded &amp; Maintained.</span>--}}
                                            <h4>What payment methods are accepted?</h4>
                                        </div>
                                        <p>We accept <b>all major credit cards</b>, including Visa, American Express, Discover, Mastercard, JBC, and Diner's Club.</p>
                                    </div>

                                    <div class="col-lg-6 bottommargin">
                                        {{--<i class="i-plain color i-large icon-line2-equalizer inline-block" style="margin-bottom: 15px;"></i>--}}
                                        <div class="heading-block nobottomborder" style="margin-bottom: 15px;">
                                            {{--<span class="before-heading">Flexible &amp; Customizable.</span>--}}
                                            <h4>What happens when the plan ends?</h4>
                                        </div>
                                        <p>Nothing! Our system keeps your account in good standing so you don't need to worry about renewing anything!</p>
                                    </div>

                                    <div class="col-lg-6 bottommargin">
                                        {{--<i class="i-plain color i-large icon-line2-equalizer inline-block" style="margin-bottom: 15px;"></i>--}}
                                        <div class="heading-block nobottomborder" style="margin-bottom: 15px;">
                                            {{--<span class="before-heading">Flexible &amp; Customizable.</span>--}}
                                            <h4>Is it for me?</h4>
                                        </div>
                                        <p>Instagram is the fastest growing social media platform with over 700 million users. This means that we can reach any target audience regardless of your industry. Whether you’re a small local business or a large online brand, we’ve got you covered.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="section parallax nomargin dark"
                         style="display:none; background-image: url('{{ asset('canvas/one-page/images/page/slider-2.jpg') }}'); padding: 150px 0;"
                         data-bottom-top="background-position:0px 0px;"
                         data-top-bottom="background-position:0px -300px;">

                        <div class="container center clearfix">

                            <div class="heading-block">
                                <h2 class="divcenter center">Hear from our clients</h2>
                                <div class="clear"></div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6 bottommargin">
                                    <div class="testimonial">
                                        <div class="testi-image">
                                            <a href="#"><img src="{{ asset('canvas/images/testimonials/1.jpg') }}"
                                                             alt="Customer Testimonails"></a>
                                        </div>
                                        <div class="testi-content">
                                            <p>Incidunt deleniti blanditiis quas aperiam recusandae consequatur ullam
                                                quibusdam cum libero illo rerum repellendus!</p>
                                            <div class="testi-meta">
                                                John Doe
                                                <span>XYZ Inc.</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 bottommargin">
                                    <div class="testimonial">
                                        <div class="testi-image">
                                            <a href="#"><img src="{{ asset('canvas/images/testimonials/3.jpg') }}"
                                                             alt="Customer Testimonails"></a>
                                        </div>
                                        <div class="testi-content">
                                            <p>Natus voluptatum enim quod necessitatibus quis expedita harum provident
                                                eos
                                                obcaecati id culpa corporis molestias.</p>
                                            <div class="testi-meta">
                                                Collis Ta'eed
                                                <span>Envato Inc.</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section><!-- #content end -->

        @include('fragments.footer')

    </div><!-- #wrapper end -->
@endsection