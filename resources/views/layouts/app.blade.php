<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<script src="{{ asset('/canvas/js/jquery.js') }}"></script>
@yield('title')

    <body class="stretched side-push-panel">

        <div class="body-overlay"></div>

        @yield('side-panel')

        @yield('content-wrapper')

        <!-- Go To Top
        ============================================= -->
        <div id="gotoTop" class="icon-angle-up"></div>

        <!-- External JavaScripts
        ============================================= -->

        <script src="{{ asset('/canvas/js/plugins.js') }}"></script>

        <!-- Footer Scripts
        ============================================= -->
        <script src="{{ asset('/canvas/js/functions.js') }}"></script>

    </body>
</html>