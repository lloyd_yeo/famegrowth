@extends('layouts.app')

@section('title')
    @include('fragments.head', [
        'title' => 'Subscribe',
    ])
@endsection

@section('side-panel')
    @include('fragments.side-panel')
@endsection

@section('content-wrapper')
    <div id="wrapper" class="clearfix">

        @include('fragments.navigation')
        <style>
            .pricing-box .col_two_third {float:none;margin: 0 auto;}
            .col_two_third input{border:none;}
            #dropin-container, .submit-next, .submit-subscribe{margin-top: 10px;}
            .card-label{
                display: block;
                border-top: solid 1px #DEE2E5;
                border-bottom: solid 1px #DEE2E5;
                padding: 8px 0;
                margin-top: -1px;
                margin-bottom: 0;
                z-index: 1;
                position: relative;
                overflow: hidden;
                -webkit-transform: translate3d(0,0,0);
                -moz-transform: translate3d(0,0,0);
                -ms-transform: translate3d(0,0,0);
                -o-transform: translate3d(0,0,0);
                transform: translate3d(0,0,0);
                -webkit-transition: background-color 131ms linear;
                -moz-transition: background-color 131ms linear;
                transition: background-color 131ms linear;
            }
            .card-field {
                display: block;
                font-weight: 200;
                font-size: 16px;
                height: 30px;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
                margin: 16px 0 0;
                padding: 0 14px;
                width: 100%;
                outline: 0;
                border: none;
                background-color: transparent;
                background-image: none;
                -webkit-tap-highlight-color: transparent;
            }
            #dropin-container, .submit-subscribe{display: none;}
            .pricing-box.pricing-extended .pricing-meta,
            .pricing-box.pricing-extended .pricing-action-area .pricing-price span.price-tenure{
                color: #524e4e;
                font-weight: 500;
            }
            .pricing-box.pricing-extended .pricing-meta{
                padding-top: 20px;
            }
            .pricing-box.pricing-extended .pricing-action-area .pricing-price{
                color: #5bc57a;
            }
            .pricing-price{
                font-size: 50px;
            }
            .float-left{
                float: left;
            }
            .float-right{
                float: right;
            }
            .padding-top-10{
                padding-top: 10px;
            }
            .total-row{
                font-size: 20px;
                min-height: 50px;
            }
            .i-plain{
                font-size: 15px;
                margin: 0;
                border: 1px solid #d6d1d1;
                border-radius: 50%;
            }
            .lh-28 .i-plain{
                line-height: 28px !important;
                height: 28px !important;
                width: 28px !important;
            }
            .lh-28{
                line-height: 28px;
                padding: 2px;
            }
            .pricing-box.pricing-extended{
                min-height: 450px;
                height: auto;
            }
            .styled-link{
                font-size: 16px;
                color: #5bc57a;
                line-height: 35px;
            }
            .nobottommargin .button{
                background-color: #5bc57a !important;
            }

        </style>
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix">
                    <div class="accordion accordion-lg divcenter nobottommargin clearfix">
                        <div class="pricing-box pricing-extended bottommargin clearfix">
                            <div class="pricing-desc">
                                <div class="pricing-title">
                                    <h3>Secure checkout</h3>
                                </div>
                                <div class="col_two_third">
                                    <form action="{{ url('/subscribe') }}" method="post" class="nobottommargin" id="subscribe-form">
                                        <div id="main-info">
                                            <label for="" class="card-label">
                                                <input name="name" type="text" placeholder="Your Name" class="card-field" required>
                                            </label>
                                            <label for="" class="card-label">
                                                <input name="email" type="email" placeholder="Your Email"  class="card-field" required>
                                            </label>
                                            <label for="" class="card-label">
                                                <input name="password" type="password" placeholder="Your Password" class="card-field" required>
                                            </label>
                                        </div>
                                        <div id="dropin-container"></div>
                                        <input type="hidden" name="plan" value="{{ $plan->id }}">
                                        {{ csrf_field() }}

                                        <div class="col_full nobottommargin submit-next">
                                            <button class="button button-3d button-black nomargin " type="button">
                                                Next
                                            </button>
                                        </div>
                                        <div class="col_full nobottommargin submit-subscribe">
                                            <button class="button button-3d button-black nomargin" type="submit">
                                                Subscribe
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="pricing-action-area">
                                <div class="">
                                    <h3>Your order</h3>
                                </div>
                                <div class="pricing-meta border-top">
                                    Package <br> <b>{{ $plan->name }}</b>
                                </div>
                                <div class="pricing-price">
                                    <span class="price-unit">$</span>{{ $plan->cost }}<span class="price-tenure">monthly</span>
                                </div>
                                <div class="border-top padding-top-10 total-row">
                                    <b class="float-left">Total</b> <b class="float-right">${{ $plan->cost }}</b>
                                </div>
                                <div class="text-left lh-28">
                                    <i class="i-plain icon-dollar"></i>&nbsp;30-day guarantee
                                </div>
                                <div class="text-left lh-28">
                                    <i class="i-plain icon-remove"></i>&nbsp;Cancel anytime
                                </div>
                                <div class="lh-28">
                                    <a href="/pricing" class="styled-link">Change</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    @include('fragments.footer')
    <script src="https://js.braintreegateway.com/js/braintree-2.30.0.min.js"></script>
    <script>
        window.onload = function(){
            $.ajax({
                url: '{{ url('braintree/token') }}'
            }).done(function (response) {
                braintree.setup(response.data.token, 'dropin', {
                    container: 'dropin-container',
                    onReady: function (integration) {},
                });
            });


            $('.submit-next button').on('click', function(event){
                if(!$('#main-info input').valid()){
                    event.preventDefault();
                    event.stopPropagation();
                    return;
                }
                $('#main-info, .submit-next').hide();
                $('#dropin-container, .submit-subscribe').show();
            });
        }

    </script>
@endsection