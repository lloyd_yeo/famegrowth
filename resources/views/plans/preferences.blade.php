@extends('layouts.app')

@section('title')
    @include('fragments.head', [
        'title' => 'Subscribe',
    ])
@endsection

@section('side-panel')
    @include('fragments.side-panel')
@endsection


@section('content-wrapper')
    <div id="wrapper" class="clearfix">
        <style>
            .button.button-desc.button-dark.button-rounded{
                text-align: left;
                padding: 24px 120px;
                font-size: 28px;
                height: auto;
                line-height: 1;
                font-family: Raleway,sans-serif;
                background-color: #1abc9c;
            }
            .button.button-desc i{
                font-size: 29px;
            }
            .button.button-desc.button-dark.button-rounded {
                border-radius: 0.3rem;
                -webkit-transition: all .2s ease-in-out;
                -o-transition: all .2s ease-in-out;
                transition: all .2s ease-in-out;
            }
            .button.button-desc.button-dark.button-rounded:hover{background-color:#59ba41;}
            .link-leave-as-is{margin: 30px auto;font-size: 24px;}
        </style>

    {{--@include('fragments.navigation')--}}

    <section id="content">
        @include('fragments.copyright')
        <div class="content-wrap">
            <div style="height:auto;" class="portfolio-single-image portfolio-single-image-full">
                <div class="video-wrap">
                    <iframe width="560" height="auto" src="https://www.youtube.com/embed/BbeOrievE2U" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
            <div class="container center">
                <div class="row divcenter clearfix">
                    <div class="col-lg-3"></div>
                    <div class="col-lg-6">
                        <div class="">
                            <a  class="button button-desc button-dark button-rounded" href="{{ url("/planUp") }}"><i class="icon-ok"></i>Yes! Sign Me Up</a>
                        </div>
                        <div class="link-leave-as-is">
                            <a href="{{ url("/") }}" class="">No thanks, I do not want to go viral</a>
                        </div>
                    </div>
                    <div class="col-lg-3"></div>
                </div>
            </div>

        </div>
    </section>

@endsection