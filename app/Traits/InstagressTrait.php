<?php

namespace App\Traits;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

trait InstagressTrait
{
    private function initGuzzleClient()
    {
        return new Client([
            // Base URI is used with relative requests
            'base_uri'    => $this->endpoint(),
            'headers'     => [
                'Content-Type' => 'application/json',
                'Accept'       => 'application/json',
            ],
            'http_errors' => FALSE,
        ]);
    }

    private function endpoint()
    {
        return config('instagress.base_endpt');
    }

    private function apiToken()
    {
        return config('instagress.api_token');
    }

    private function id()
    {
        return config('instagress.id');
    }

    public function searchUsername($query) {
        $client = $this->initGuzzleClient();

        //Set activity for profile
        $raw_response = $client->get('/api/activity/settings/usernames/search?token=' . $this->apiToken() . '&id=' . $this->id() . '&q=' . $query);

        $response = json_decode($raw_response->getBody());

        if ($response->ok) {
             return collect($response->usernames)
                        ->take(100)
                        ->reject(function ($item, $key) {
                            return empty($item->full_name);
                        })
                        ->take(10);
        }

        return [];
    }

    public function searchHashtag($query) {
        $client = $this->initGuzzleClient();

        $raw_response = $client->get('/api/activity/settings/tags/search?token=' . $this->apiToken() . '&id=' . $this->id() . '&q=' . $query);

        $response = json_decode($raw_response->getBody());

        if ($response->ok) {
            return collect($response->tags)->take(10);
        }

        return [];
    }

    public function login($username, $password)
    {
        $client = $this->initGuzzleClient();

        $raw_response = $client->post('/api/accounts/connect', [
            RequestOptions::JSON => [
                'token'    => $this->apiToken(),
                'username' => $username,
                'password' => $password,

            ],
        ]);

        $response = json_decode($raw_response->getBody());

        return $response;
    }
    public function hasUserId($user)
    {
        if(!$user){
            return auth()->user()->instagram_datas->instagress_user_id;
        }

        return $user->instagram_datas->instagress_user_id;
    }

    public function setUserId($user, $page = null)
    {
        $response = $this->accounts($page);

        if($response->ok) {
            $result = $this->set($response, $user);

            if(!$result && $response->pagination->totalPages > 1 && $response->pagination->totalPages >= $page){
                return $this->setUserId($user, $page += 1);
            }
        }
    }

    public function accounts($user, $page = null)
    {
        $client = $this->initGuzzleClient();
        $url = '/api/accounts?token=' . $this->apiToken();

        if($page){
            $url .= '&page='. $page;
        }

        $raw_response = $client->get($url);

        return json_decode($raw_response->getBody());
    }

    public function set($response, $user)
    {
        foreach($response->accounts as $account){
            if($account->username == $user->instagram_datas->instagram_username){
                $user->instagram_datas->instagress_user_id = $account->_id;
                $user->instagram_datas->save();
                return true;
            }
        }

        return false;
    }

    public function instagressLike($like, $id, $tags = [], $users = [])
    {
        $client = $this->initGuzzleClient();

        $raw_response = $client->post('/api/activity/settings/set', [
            RequestOptions::JSON => [
                'token'    => $this->apiToken(),
                'id' => $id,
                'settings' => [
                    'like' => $like,
                    'tags' => $tags,
//                    'usernames' => $users
                ]
            ],
        ]);

        $response = json_decode($raw_response->getBody());

        return $response->ok;
    }

    public function instagressFollow($follow, $id, $tags = [], $users = [])
    {
        $client = $this->initGuzzleClient();

        $raw_response = $client->post('/api/activity/settings/set', [
            RequestOptions::JSON => [
                'token'    => $this->apiToken(),
                'id' => $id,
                'settings' => [
                    'follow' => $follow,
                    'unfollow' => !$follow,
                    'tags' => $tags,
//                    'usernames' => $users
                ]
            ],
        ]);

        $response = json_decode($raw_response->getBody());

        return $response->ok;
    }


}
