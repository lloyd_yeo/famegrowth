<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstagramData extends Model
{
    protected $guarded = [];

    protected $casts = [
        'users' => 'array',
        'tags' => 'array',
    ];

    public function user()
    {
        return $this->hasOne(User::class);
    }
}
