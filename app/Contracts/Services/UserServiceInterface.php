<?php

namespace App\Contracts\Services;


interface UserServiceInterface
{
    public function getUser(array $data);

    public function lastSubscription($user);
}