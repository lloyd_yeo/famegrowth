<?php

namespace App\Contracts\Repositories;

interface RepositoryInterface
{
    public function create(array $data);

    public function all($columns = ['*']);

    public function get($paginate = null, $columns = ['*']);

    public function by($field, $value);
}