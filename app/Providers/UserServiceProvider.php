<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class UserServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        /* repositories */
        $this->app->bind(
            'App\Contracts\Repositories\UserRepositoryInterface',
            'App\Repositories\Eloquent\UserRepository'
        );

        /* services */
        $this->app->bind(
            'App\Contracts\Services\UserServiceInterface',
            'App\Services\UserService'
        );
    }
}
