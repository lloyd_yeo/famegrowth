<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $fillable = [
                'name',
                'braintree_id',
                'quantity',
                'trial_ends_at',
                'braintree_plan',
                'ends_at'
            ];
}
