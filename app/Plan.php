<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $fillable = [
        'name',
        'slug',
        'braintree_plan',
        'cost',
        'description',
        'trialDuration',
        'trialDurationUnit',
        'trialPeriod',
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function scopeNext($query, $params)
    {
        return $query->when($params->braintree_plan, function($query) use ($params){
                   $query->where('braintree_plan', '!=' , $params->braintree_plan);
                   $query->where('cost', '>', $this->nextCost($params->braintree_plan));
                });
    }

    public function nextCost($plan)
    {
        return $this->where('braintree_plan', $plan)->first()->cost;
    }

}
