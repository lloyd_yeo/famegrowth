<?php

namespace App\Repositories\Eloquent;

use Illuminate\Container\Container;
use App\Contracts\Repositories\RepositoryInterface;

abstract class Repository implements RepositoryInterface
{
    protected $model;

    public function __construct(Container $container)
    {
        $this->model = $container->make($this->model());
    }

    protected abstract function model();

    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function all($columns = ['*'])
    {
        return $this->model->get($columns);
    }

    public function get($paginate = null, $columns = ['*'])
    {
        return $this->model->paginate($paginate, $columns);
    }

    public function find($id, $columns = ['*'])
    {
        return $this->model->findOrFail($id, $columns);
    }

    public function by($field, $value)
    {
        return $this->model->where($field, $value)->first();
    }
}