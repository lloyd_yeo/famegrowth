<?php

namespace App\Repositories\Eloquent;

use App\User;
use App\Contracts\Repositories\UserRepositoryInterface;


class UserRepository extends Repository implements UserRepositoryInterface
{
    protected function model()
    {
        return User::class;
    }
}