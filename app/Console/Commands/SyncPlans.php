<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Braintree_Plan;
use App\Plan;

class SyncPlans extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'braintree:sync-plans';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync with online plans on Braintree';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Plan::truncate();
        $braintreePlans = collect(Braintree_Plan::all());
        $existingPlans = config("braintree.plans.".config("app.env"));

        $famegrowthPlans = $braintreePlans->filter(function ($item, $key) use ($existingPlans) {
            return in_array($item->id, $existingPlans);
        });

        foreach ($famegrowthPlans as $braintreePlan) {
            Plan::create([
                'name' => $braintreePlan->name,
                'slug' => str_slug($braintreePlan->id),
                'braintree_plan' => $braintreePlan->id,
                'cost' => $braintreePlan->price,
                'description' => $braintreePlan->description,
                'trialDuration' => $braintreePlan->trialDuration,
                'trialDurationUnit' => $braintreePlan->trialDurationUnit,
                'trialPeriod' => $braintreePlan->trialPeriod,
            ]);
        }
    }
}
