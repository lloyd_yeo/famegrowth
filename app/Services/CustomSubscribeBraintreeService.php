<?php

namespace App\Services;

use App\Plan;
use Illuminate\Support\Facades\Auth;
use Braintree\Subscription as BraintreeSubscription;
use App\Contracts\Services\UserServiceInterface as UserService;

class CustomSubscribeBraintreeService
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function subscribe($params)
    {
        $plan = Plan::findOrFail(request()->plan);
        $user = $this->userService->getUser($params);
        Auth::login($user, true);

        $transaction = $user->newSubscription('main', $plan->braintree_plan);
        $transaction->create(request()->payment_method_nonce, [], ['merchantAccountId' => env('BRAINTREE_ACCOUNT_ID')]);
    }

    public function planUp()
    {
        $user = null;

        if(!$user = Auth::user()){
            return redirect('pricing');
        }

        $subscription = $this->userService->lastSubscription($user);
        $plan = $this->nextPlan($subscription);

        if($plan){
            $this->swapPlan($subscription, $plan, $user);
        }

        return redirect('/');
    }

    protected function nextPlan($subscription)
    {
        return Plan::orderBy('cost', 'asc')
            ->next($subscription)
            ->first();
    }

    protected function swapPlan($subscription, $plan, $user)
    {
        $response = BraintreeSubscription::update($subscription->braintree_id, [
            'planId' => $plan->braintree_plan,
            'price'  => (string) round($plan->cost * (1 + ($user->taxPercentage() / 100)), 2),
            'merchantAccountId' => env('BRAINTREE_ACCOUNT_ID')
        ]);

        if ($response->success) {
            $subscription->fill([
                'braintree_plan' => $plan->braintree_plan,
                'ends_at' => null,
            ])->save();
        } else {
            throw new Exception('Braintree failed to swap plans: '.$response->message);
        }
    }
}