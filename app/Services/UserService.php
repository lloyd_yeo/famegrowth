<?php

namespace App\Services;

use Illuminate\Support\Facades\Hash;
use App\Contracts\Services\UserServiceInterface;
use Illuminate\Support\Facades\Auth;
use App\Contracts\Repositories\UserRepositoryInterface as UserRepository;

class UserService implements UserServiceInterface
{
    protected $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getUser(array $data)
    {
//        if(Auth::id()){
//            return Auth::user();
//        }

        return $this->user($data);
    }


    protected function user($data)
    {
        $user = null;

        if(!empty($data['email'])){
            $user = $this->by('email', $data['email']);
        }

        if(!$user){
            $user = $this->makeUser($data);
        }

        return $user;
    }

    protected function by($field, $value)
    {
        return $this->repository->by($field, $value);
    }

    protected function makeUser($data)
    {
        $params = [
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ];

        return $this->repository->create($params);
    }

    public function lastSubscription($user)
    {
        return $user->subscriptionList()->latest()->first();
    }
}