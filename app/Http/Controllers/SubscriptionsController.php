<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Exception;
use App\Plan;
use Illuminate\Support\Facades\Auth;
use App\Contracts\Services\UserServiceInterface as UserService;
use Braintree\Subscription as BraintreeSubscription;

class SubscriptionsController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function store(Request $request)
    {
        $params = $request->all();
//        dump($params);
        $plan = Plan::findOrFail($request->plan);

        $user = $this->userService->getUser($params);
//        dump($user);

//        $user->newSubscription('main', $plan->braintree_plan)->create($request->payment_method_nonce);

        Auth::login($user, true);

        return redirect("/plans-preferences");
    }

    public function planUp()
    {
        $user = null;

        if(!$user == Auth::user()){
            return redirect('pricing');
        }

        $subscription = $this->userService->lastSubscription($user);
        $plan = $this->nextPlan($subscription);

        if($plan){
            $user->subscription('main')->swap($plan->braintree_plan);
        }

        return redirect('/');
    }

    protected function nextPlan($subscription)
    {
        return Plan::orderBy('cost', 'asc')
                ->next($subscription)
                ->first();
    }

}
