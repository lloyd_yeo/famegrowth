<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

class InstagressController extends Controller
{
    public $base_endpt = 'https://gress.io';
    public $api_token = '1d73c7c1b10f05f2048f54083e9381ac18f36261a98416a00b7eed6b00d56eb4';

    public function testInstagress(Request $request)
    {

        $client = $this->initGuzzleClient();

        $response = $client->post('/api/ping', [
            RequestOptions::JSON => [ 'token' => $this->api_token ],
        ]);

        $json_response = json_decode($response->getBody());

        dump($json_response);
        //dump("OK => " . $json_response->ok);
        //dump("PONG => " . ($json_response->pong));
    }

    public function connect(Request $request)
    {
        $username = $request->input('username');
        $password = $request->input('password');

        if (InstagramProfile::where('insta_username', $username)->first() != NULL) {
            return response()->json([
                'success' => FALSE,
                'message' => 'Acest Profil de Instagram a fost adaugat deja!',
            ]);
        }

        $profile_response = $this->searchAccount($username);

//		dump($profile_response);

        if ($profile_response->ok) {
            $accounts = $profile_response->accounts;
            foreach ($accounts as $account) {
                if ($account->username == $username) {
                    $this->storeProfile($account, $username, $password);

                    return response()->json([ 'success' => TRUE,
                        'message' => 'Account has been successfully connected.',
                    ]);
                }
            }
        }

        $profile_log = new CreateInstagramProfileLog();

        if (Auth::check()) {
            $email = Auth::user()->email;
        }

        $profile_log->email          = $email;
        $profile_log->insta_username = $username;
        $profile_log->insta_pw       = $password;
        $profile_log->save();

        $response = $this->login($username, $password);

        $profile_log->error_msg = json_encode($response);
        $profile_log->save();

        // Log::info(json_encode($response));

        if ($response->ok) {

            $user = $response->user;

            $this->storeProfile($user, $username, $password);

            return response()->json([ 'success' => TRUE,
                'message' => 'Account has been successfully connected.',
            ]);

        } else {

            $error = $response->error;

            if ($error->type == 'ApiRequestError') {
                return $this->handleApiRequestError($error, $profile_log);
            }
        }
    }

    public function searchAccount($query) {
        $client = $this->initGuzzleClient();

        $raw_response = $client->get('/api/accounts?token=' . $this->api_token . '&search=' . $query);

        $response = json_decode($raw_response->getBody());

        return $response;
    }


    public function verify(Request $request)
    {
        $verification_code = $request->input('verification_code');
        $profile_log_id    = $request->input('profile_log_id');
        $login_data_token  = $request->input('login_data_token');

        if ($profile_log_id != NULL) {

            $profile_log = CreateInstagramProfileLog::find($profile_log_id);

            $response = $this->sendVerificationCode($verification_code, $login_data_token);

            $profile_log->error_msg = json_encode($response);
            $profile_log->save();

            if ($response->ok) {
                //'ok' will never be true because we are in the verification process now.

                return response()->json([
                    'success' => TRUE,
                    'message' => 'Numele pe care l-ai introdus nu este valid. Te rugam sa reintroduci numele de utilizator corect si sa reincerci.',
                    'variable' => json_encode($response),
                ]);
            } else {
                $error = $response->error;

                if ($error->type == 'ApiRequestError') {
                    return $this->handleApiRequestError($error, $profile_log);
                }
            }

        } else {
            return response()->json([
                'success' => FALSE,
                'message' => 'Numele pe care l-ai introdus nu este valid. Te rugam sa reintroduci numele de utilizator corect si sa reincerci.',
            ]);
        }
    }

    public function verifyTwoFactor(Request $request) {
        $twofa_code = $request->input('twofactor_code');
        $profile_log_id    = $request->input('profile_log_id');
        $login_data_token  = $request->input('login_data_token');

        if ($profile_log_id != NULL) {

            $profile_log = CreateInstagramProfileLog::find($profile_log_id);

            $response = $this->sendTwoFactorAuthCode($login_data_token, $twofa_code);

            $profile_log->error_msg = json_encode($response);
            $profile_log->save();

            if ($response->ok) {

                $user = $response->user;

                $this->storeProfile($user, $profile_log->username, $profile_log->password);

                return response()->json([ 'success' => TRUE,
                    'message' => 'Account has been successfully connected.',
                ]);

            } else {
                $error = $response->error;

                if ($error->type == 'ApiRequestError') {
                    return $this->handleApiRequestError($error, $profile_log);
                }
            }

        } else {
            return response()->json([
                'success' => FALSE,
                'message' => 'Numele pe care l-ai introdus nu este valid. Te rugam sa reintroduci numele de utilizator corect si sa reincerci.',
            ]);
        }
    }

    private function initGuzzleClient()
    {
        $client = new Client([
            // Base URI is used with relative requests
            'base_uri'    => $this->base_endpt,
            'headers'     => [
                'Content-Type' => 'application/json',
                'Accept'       => 'application/json',
            ],
            'http_errors' => FALSE,
        ]);

        return $client;
    }

    /**
     * @param $error
     * @param $profile_log
     */
    public function handleApiRequestError($error, $profile_log)
    {
        $error_data = $error->data;

        $error_code = $error_data->code;

        switch ($error_code) {
            case 'request_error':
                $error_msg = 'There is a server error. Please contact our live support.';

                return;
            case 'login_auth':

                return response()->json([ "success" => FALSE, 'type' => 'incorrect_pw', 'message' => "Parola introdusa este incorecta!" ]);

            case 'login_access':
                $error_msg = 'There is a server error. Please contact our live support.';

                return;
            case 'login_verify':

                return $this->verifyLogin($profile_log, $error_data, $error_code);

            case 'login_verify_code':

                return;

            case 'login_verify_ok':

                return $this->verifyLoginOk($profile_log);

            case 'login_verify_no_choice':

                $error_msg = 'We are unable to begin the verification process because your account does not have any email or phone number attached.';

                return;

            case 'login_twofactor':

                $this->twoFactorLogin($profile_log, $error_data, $error_code);

                return;
            case 'login_twofactor_code':

                return;
            case 'default':
                return;
        }
    }

    /**
     * @param $username
     * @param $password
     *
     * @return mixed
     */
    public function login($username, $password)
    {
        $client = $this->initGuzzleClient();

        $raw_response = $client->post('/api/accounts/connect', [
            RequestOptions::JSON => [
                'token'    => $this->api_token,
                'username' => $username,
                'password' => $password,

            ],
        ]);

        $response = json_decode($raw_response->getBody());

        return $response;
    }

    /**
     * @param $verification_code
     *
     * @return mixed
     */
    public function sendVerificationCode($verification_code, $login_data_token)
    {
        $client = $this->initGuzzleClient();

        $raw_response = $client->post('/api/accounts/connect', [
            RequestOptions::JSON => [
                'token'            => $this->api_token,
                'login_data_token' => $login_data_token,
                'verify_code'      => $verification_code,
            ],
        ]);

        $response = json_decode($raw_response->getBody());

        return $response;
    }

    /**
     * @param $profile_log
     * @param $error_data
     * @param $error_code
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifyLogin($profile_log, $error_data, $error_code)
    {
        $profile_log->error_msg = json_encode([
            'error_code'          => $error_code,
            'login_data_token'    => $error_data->loginDataToken,
            'choice_type'         => $error_data->choiceType,
            'choice_masked_label' => $error_data->choiceMaskedLabel,
        ]);

        $profile_log->save();

        if ($error_data->choiceType == 'email') {
            $message = 'Instagram necesita o verificare din partea ta. Te rugam sa verifici urmatorul E-Mail <b>' . $error_data->choiceMaskedLabel . '</b> pentru a gasi codul de 6 cifre. Vei primi un E-Mail de la Instagram pe care te rugam sa il cauti nu numai in Inbox ci si in Social/Spam.';
        } else {
            $message = 'Instagram necesita o verificare din partea ta. Te rugam sa verifici urmatorul numar de Telefon: <b>' . $error_data->choiceMaskedLabel . '</b> pentru a gasi codul de 6 cifre.';
        }

        return response()->json([
            'success'          => FALSE,
            'type'             => 'challenge',
            'message'          => $message,
            'profile_log_id'   => $profile_log->log_id,
            'login_data_token' => $error_data->loginDataToken,
        ]);
    }

    /**
     * @param $profile_log
     * @param $error_data
     * @param $error_code
     */
    public function twoFactorLogin($profile_log, $error_data, $error_code)
    {
        $profile_log->error_msg = json_encode([
            'error_code'         => $error_code,
            'login_data_token'   => $error_data->loginDataToken,
            'phone_masked_label' => $error_data->phoneMaskedLabel,
        ]);

        $profile_log->save();

        return response()->json([
            'success'            => FALSE,
            'type'               => '2fa',
            'message'            => 'Contul tau are activa optiunea 2FactorAuthenitification/Autentificare In 2 Pasi, din aceasta cauza nu se poate stabili o conexiune. Te rugam sa introduci Codul primit mai jos:',
            'profile_log_id'     => $profile_log->log_id,
            'login_data_token'   => $error_data->loginDataToken,
            'phone_masked_label' => $error_data->phoneMaskedLabel,
        ]);
    }

    /**
     * @param $profile_log
     */
    public function verifyLoginOk($profile_log)
    {
        $profile_log->error_msg = json_encode([
            'success' => TRUE,
            'message' => 'Valid verification code.',
        ]);

        $profile_log->save();

        $username = $profile_log->username;
        $password = $profile_log->password;

        $response = $this->login($username, $password);

        if ($response->ok) {

            // Log::info(json_encode($response));

            $user = $response->user;

            $this->storeProfile($user, $username, $password);

            return response()->json([ 'success' => TRUE,
                'message' => 'Account has been successfully connected.',
            ]);
        }
    }

    /**
     * @param $user
     * @param $username
     * @param $password
     */
    public function storeProfile($user, $username, $password)
    {
        $ig_profile                    = new InstagramProfile();
        $ig_profile->user_id           = Auth::user()->user_id;
        $ig_profile->email             = Auth::user()->email;
        $ig_profile->insta_username    = $username;
        $ig_profile->profile_full_name = $username;
        $ig_profile->insta_pw          = $password;
        $ig_profile->insta_user_id     = $user->_id;
        if ($user->has_profile_pic) {
            $ig_profile->profile_pic_url = $user->profile_picture;
        }
        $ig_profile->instagress = 1;
        if ($ig_profile->save()) {
            $this->addTimeToAccount($user->_id);
        }

        return $ig_profile;
    }

    public function addTimeToAccount($profile_id)
    {

        $client = $this->initGuzzleClient();

        $days_to_add = 2;

        if (Auth::user()->tier > 1) {
            $days_to_add = 7;
        }

        $raw_response = $client->post('/api/time/add', [
            RequestOptions::JSON => [
                'token'  => $this->api_token,
                'to_ids' => $profile_id,
                'days'   => $days_to_add,
            ],
        ]);

        $response = json_decode($raw_response->getBody());

        // Log::info($response);
    }

    public function addTimeToAccountTest(Request $request)
    {

        $profile_id = $request->input('profile_id');

        $client = $this->initGuzzleClient();

        $days_to_add = 2;

        if (Auth::user()->tier > 1) {
            $days_to_add = 7;
        }

        $raw_response = $client->post('/api/time/add', [
            RequestOptions::JSON => [
                'token'  => $this->api_token,
                'to_ids' => $profile_id,
                'days'   => $days_to_add,
            ],
        ]);

        $response = json_decode($raw_response->getBody());
        dump($response);
        // Log::info(json_encode($response));
    }

    public function disconnect(Request $request, $id)
    {
        $ig_profile = InstagramProfile::find($id);

        $profile_id = $ig_profile->insta_user_id;

        if ($ig_profile != NULL) {
            $stop_activity_response = $this->stopActivity($profile_id);
            if ($stop_activity_response->ok) {

                $deduct_all_time_response = $this->deductAllTime($profile_id);
                if ($deduct_all_time_response->ok) {

                    $disconnect_profile_response = $this->disconnectProfile($profile_id);
                    if ($disconnect_profile_response->ok) {

                        if ($ig_profile->delete()) {
                            return Response::json([ "success" => TRUE, 'response' => 'Profilul tau a fost sters.' ]);
                        } else {
                            return Response::json([ "success" => TRUE, 'response' => 'Contul nu poate fi sters, te rugam sa incerci mai tarziu.' ]);
                        }

                    } else {
                        return Response::json([ "success" => FALSE, 'response' => 'Failed to disconnect profile from dashboard.', $stop_activity_response->error->message ]);
                    }
                } else {
                    return Response::json([ "success" => FALSE, 'response' => 'Failed to deduct time from profile.', $stop_activity_response->error->message ]);
                }
            } else {
                return Response::json([ "success" => FALSE, 'response' => 'Failed to stop activity for profile.', $stop_activity_response->error->message ]);
            }
        } else {
            return Response::json([ "success" => FALSE, 'response' => 'Profile not found.' ]);
        }
    }

    /**
     * @param $id
     *
     * @return array
     */
    public function stopActivity($id)
    {
        $client = $this->initGuzzleClient();

        //Stop all activity on this account first
        $raw_response = $client->post('/api/activity/stop', [
            RequestOptions::JSON => [
                'token' => $this->api_token,
                'ids'   => $id,
            ],
        ]);

        $response = json_decode($raw_response->getBody());

        return $response;
    }

    /**
     * @param $id
     *
     * @return array
     */
    public function deductAllTime($id)
    {
        $client = $this->initGuzzleClient();

        //Deduct all the time on this profile first
        $raw_response = $client->post('/api/time/deduct', [
            RequestOptions::JSON => [
                'token'    => $this->api_token,
                'from_ids' => $id,
                'all'      => TRUE,
            ],
        ]);

        $response = json_decode($raw_response->getBody());

        return $response;
    }

    /**
     * @param $id
     */
    public function disconnectProfile($id)
    {
        $client = $this->initGuzzleClient();

        //Disconnect profile from Instagress
        $raw_response = $client->post('/api/accounts/disconnect', [
            RequestOptions::JSON => [
                'token' => $this->api_token,
                'id'    => $id,
            ],
        ]);

        $response = json_decode($raw_response->getBody());

        return $response;
    }

    public function updateSettings(Request $request, $id) {

        $ig_profile = InstagramProfile::find($id);

        $profile_id = $ig_profile->insta_user_id;

        $settings = array();

        //Auto-Like
        if ($ig_profile->auto_like == 1) {
            $settings['like'] = true;
        } else {
            $settings['like'] = false;
        }

        //Auto-Comment
        if ($ig_profile->auto_comment == 1) {
            $settings['comment'] = true;
        } else {
            $settings['comment'] = false;
        }

        //Auto-Follow
        if ($ig_profile->auto_follow == 1) {
            $settings['follow'] = true;
        } else {
            $settings['follow'] = false;
        }

        //Auto-Unfollow
        if ($ig_profile->auto_unfollow == 1) {
            $settings['unfollow'] = true;
        } else {
            $settings['unfollow'] = false;
        }

        //Interaction Speed
        if ($ig_profile->speed == "Medium") {
            $settings['speed'] = 'normal';
        } else {
            $settings['speed'] = strtolower($ig_profile->speed);
        }

        $settings['speedAlgo'] = 'a1';

        $usernames = array();
        $hashtags = array();

        $target_username_set = 0;
        $target_hashtag_set = 0;

        if ($ig_profile->niche > 0) {
            //target usernames & hashtags from selected niche
            $niche = Niche::find($ig_profile->niche);
            if ($niche != NULL) {
                $niche_target_usernames = NicheTarget::where('niche_id', $niche->id)->get();

                foreach ($niche_target_usernames as $niche_target_username) {
                    $username = $this->searchUsername($niche_target_username->target_username, $profile_id);
                    if ($username != NULL) {
                        $usernames[] = $username;
                        $target_username_set = 1;
                    }
                }

                $niche_target_hashtags = NicheTargetHashtag::where('niche_id', $niche->id)->get();

                foreach ($niche_target_hashtags as $niche_target_hashtag) {
                    $hashtag = $this->searchUsername($niche_target_hashtag->hashtag, $profile_id);
                    if ($hashtag != NULL) {
                        $hashtags[] = $hashtag;
                        $target_hashtag_set = 1;
                    }
                }

            }
        } else {
            //use custom-targeted username & hashtags
            $target_usernames = InstagramProfileTargetUsername::where('insta_username', $ig_profile->insta_username)->get();

            foreach ($target_usernames as $target_username) {
                $username = $this->searchUsername($target_username->target_username, $profile_id);
                if ($username != NULL) {
                    $usernames[] = $username;
                    $target_username_set = 1;
                }
            }

            $target_hashtags = InstagramProfileTargetHashtag::where('insta_username', $ig_profile->insta_username)->get();

            foreach ($target_hashtags as $target_hashtag) {
                $hashtag = $this->searchUsername($target_hashtag->hashtag, $profile_id);
                if ($hashtag != NULL) {
                    $hashtags[] = $hashtag;
                    $target_hashtag_set = 1;
                }
            }
        }

        if ($target_username_set == 1) {
            $settings['targetFollowers'] = 'user';
            $settings['targetFollowings'] = 'user';
            $settings['targetLikers'] = 'user';
            $settings['targetCommenters'] = 'user';
            $settings['usernames'] = $usernames;
        }

        if ($target_hashtag_set == 1) {
            $settings['targetTags'] = true;
            foreach ($hashtags as $hashtag) {
                $settings['tags'] = $hashtag->name;
            }
        }

        $settings['userProfileFilter'] = 'f';
        $settings['unfollowSource'] = 'db';
        $settings['followCycle'] = $ig_profile->follow_cycle;

//		dump("[SETTINGS VAR]");
//		dump($settings);

        $settings = json_decode(json_encode($settings), FALSE);

//		dump("[SETTINGS VAR - CASTED]");
//		dump($settings);

        $client = $this->initGuzzleClient();

        //Set activity for profile
        $raw_response = $client->post('/api/activity/settings/set', [
            RequestOptions::JSON => [
                'token' => $this->api_token,
                'id'    => $profile_id,
                'settings' => $settings,
            ],
        ]);

//		dump("[SETTING ACTIVITY]");
//		dump($raw_response);
        $response = json_decode($raw_response->getBody());
//		dump("[SETTING ACTIVITY BODY]");
//		dump($response);

        if ($response->ok) {

            if ($target_hashtag_set || $target_username_set) {
                if ($this->startActivity($profile_id)) {
                    return response()->json(['success' => TRUE, 'message' => 'Profile started.']);
                }
            }

            return response()->json(['success' => TRUE, 'message' => 'Settings updated.']);
        }
    }

    public function startActivity($id) {
        $client = $this->initGuzzleClient();

        //Start activity for profile
        $raw_response = $client->post('/api/activity/start', [
            RequestOptions::JSON => [
                'token' => $this->api_token,
                'ids'    => $id
            ],
        ]);

        $response = json_decode($raw_response->getBody());

        if ($response->ok) {
            return TRUE;
        } else {
            return $response;
        }
    }

    public function searchHashtag($query, $id) {
        $hashtag = NULL;
        $client = $this->initGuzzleClient();

//		dump("[HASHTAG SEARCH QUERY]");
//		dump($query);
//		dump($id);

        //Set activity for profile
        $raw_response = $client->get('/api/activity/settings/tags/search?token=' . $this->api_token . '&id=' . $id . '&q=' . $query);

//		dump("[HASHTAG SEARCH]");
//		dump($raw_response);

        $response = json_decode($raw_response->getBody());

//		dump("[HASHTAG SEARCH BODY]");
//		dump($response);

        if ($response->ok) {
            $hashtags = $response->tags;
            foreach ($hashtags as $hashtag) {
                return $hashtag;
            }
        }
        return $hashtag;
    }

    public function searchUsername($query, $id) {
        $username = NULL;
        $client = $this->initGuzzleClient();

//		dump("[USERNAME SEARCH QUERY]");
//		dump($query);
//		dump($id);

        //Set activity for profile
        $raw_response = $client->get('/api/activity/settings/usernames/search?token=' . $this->api_token . '&id=' . $id . '&q=' . $query);

//		dump("[USERNAME SEARCH]");
//		dump($raw_response);

        $response = json_decode($raw_response->getBody());

//		dump("[USERNAME SEARCH BODY]");
//		dump($response);

        if ($response->ok) {
            $usernames = $response->usernames;
            foreach ($usernames as $username) {
                return $username;
            }
        }
        return $username;
    }

    /**
     * @param $login_data_token
     * @param $twofa_code
     *
     * @return mixed
     */
    public function sendTwoFactorAuthCode($login_data_token, $twofa_code)
    {
        $client = $this->initGuzzleClient();

        $raw_response = $client->post('/api/accounts/connect', [
            RequestOptions::JSON => [
                'token'            => $this->api_token,
                'login_data_token' => $login_data_token,
                'twofactor_code'   => $twofa_code,
            ],
        ]);

        $response = json_decode($raw_response->getBody());

        return $response;
    }
}
