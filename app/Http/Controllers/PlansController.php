<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Plan;
use Illuminate\Support\Facades\Auth;

class PlansController extends Controller
{
    public function index()
    {
        return view('plans.index')->with(['plans' => Plan::get()]);
    }

    public function show(Plan $plan)
    {
        return view('plans.show')->with(compact('plan'));
    }

    public function preferences()
    {
        if(!Auth::user()){
            return redirect('/');
        }

        return view('plans.preferences');
    }

}
