<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\InstagressTrait;

class RegisterInstagressController extends Controller
{
    use InstagressTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('instagress.index');
    }

    public function checkEmail(Request $request)
    {
        $error = ['error' => true];
        if(!$request->email) return response($error);

        if(!$this->validEmail($request->email)){
            return response($error);
        }

        return response(['success' => true]);
    }

    protected function validEmail($email)
    {
        return auth()->user()->email == $email;
    }


    public function instagramUsers(Request $request)
    {
        return $this->searchUsername($request->search);
    }

    public function storeRelatedUsers(Request $request)
    {
        $users = array_where($request->users, function($item, $key){
            return !empty($item['full_name']);
        });

        $this->storeData(['users' => $users]);

        return response(['success' => true]);
    }

    public function instagramTags(Request $request)
    {
        return $this->searchHashtag($request->search);
    }

    public function storeRelatedTags(Request $request)
    {
        $tags = array_where($request->tags, function($item, $key){
            return !empty($item['name']);
        });

        $this->storeData(['tags' => $tags]);

        return response(['success' => true]);
    }

    public function storeFollow(Request $request)
    {
        $this->storeData(['follow' => $request->follow]);

        return response(['success' => true]);
    }

    public function instagramCredentials(Request $request)
    {
        $this->storeData([
            'instagram_username' => $request->username,
            'instagram_password' => $request->password
        ]);

        $result = $this->login($request->username, $request->password);

        return response(['ok' => $result->ok]);
    }

    private function storeData($attributes)
    {
        $user = auth()->user();

        if(!$user->instagram_datas){
            return $user->instagram_datas()->create($attributes);
        }

        return $user->instagram_datas->update($attributes);
    }

    public function saveTargets(Request $request)
    {
        dd($request->all());
    }

}
