<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\InstagressTrait;

class DashboardController extends Controller
{
    use InstagressTrait;


    public function index()
    {
        $user = auth()->user();

        if(!$this->hasUserId($user)){
            $this->setUserId($user);
        }

        return view('dashboard.index', compact('user'));
    }

    public function data()
    {
        return $this->instagramData();
    }

    protected function instagramData()
    {
        $userInfo = auth()->user()->instagram_datas;

        $data = array_collapse(
                    $userInfo->only(['users', 'tags'])
                );

        if(empty($data)){
            return [];
        }

        return $this->sortData($data);
    }

    protected function sortData($data)
    {
        $collected = collect($data)->map(function ($item, $key) {
            if(isset($item['full_name'])){
                $item['name'] = $item['full_name'];
            }

            return $item;
        });

        return $collected->sortBy('name')->values();
    }

    public function liking(Request $request)
    {
        if(!$request->has('liking')){
            return;
        }

        $userInfo = auth()->user()->instagram_datas;
        $liking = $request->liking;
        $tags = array_pluck($userInfo->tags, 'name');

        $result = $this->instagressLike($liking, $userInfo->instagress_user_id, $tags);

        if($result){
            $userInfo->liking = $liking;
            $userInfo->save();
        }
    }

    public function following(Request $request)
    {
        if(!$request->has('follow')){
            return;
        }

        $userInfo = auth()->user()->instagram_datas;
        $follow = $request->follow;
        $tags = array_pluck($userInfo->tags, 'name');
//        $users = array_pluck($data->users, 'username');

        $result =  $this->instagressFollow($follow, $userInfo->instagress_user_id, $tags);

        if($result){
            $userInfo->follow = $follow;
            $userInfo->save();
        }
    }


    public function deleteRow(Request $request)
    {
        if(!$request->has('row') && !$request->has('type')){
            return;
        }

        $id = $request->row;
        $type = $request->type;

        $userInfo = auth()->user()->instagram_datas;

        $filtered = array_where($userInfo->$type, function ($item, $key) use ($id) {
            if(isset($item['id'])){
                return $item['id'] !== $id;
            }
        });

        $userInfo->$type = $filtered;
        $userInfo->save();
    }


    public function saveTargets(Request $request)
    {
        $tags = $this->filterData('name', $request->tags);
        $users = $this->filterData('full_name', $request->users);
        $user = auth()->user();

        if(!empty($tags)){
            $this->updateList($user, $tags, 'tags');
        }

        if(!empty($users)){
            $this->updateList($user, $users, 'users');
        }

        return response([
                'success' => true,
                'list' => $this->data()
            ]);
    }

    protected function filterData($field, $data = []){
        return array_where($data, function($item, $key) use ($field){
            return !empty($item[$field]);
        });
    }

    protected function updateList($user, $data, $field)
    {
        $list = $user->instagram_datas->$field;
        $all_values = array_merge($list, $data);
        $user->instagram_datas->$field = $this->removeDublicates($all_values);
        $user->instagram_datas->save();
    }

    protected function removeDublicates($data = [])
    {
        if(empty($data)){
            return [];
        }

        $ids = array_column($data, 'id');
        $ids = array_unique($ids);
        return array_filter($data, function ($key, $value) use ($ids) {
                    return in_array($value, array_keys($ids));
               }, ARRAY_FILTER_USE_BOTH);

    }


}
