<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Auth::loginUsingId(1);
Route::get('/', 'HomeController@welcome')->name('welcome');

Route::get('/pricing', 'HomeController@pricing')->name('pricing');

Route::get('/member/get-started', 'HomeController@getStarted')->name('getStartedOnBoarding');
Route::post('/member/get-started', 'HomeController@showGetStartedPayment')->name('showGetStartedPaymentForm');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/plans', 'PlansController@index');
Route::get('/plan/{plan}', 'PlansController@show');
Route::get('/plans-preferences', 'PlansController@preferences');
Route::get('/braintree/token', 'BraintreeTokenController@token');
Route::post('/subscribe', 'SubscriptionsController@store');
Route::get('/planUp', 'SubscriptionsController@planUp');


Route::get('/connect', 'RegisterInstagressController@index');
Route::get('/check-email', 'RegisterInstagressController@checkEmail');
Route::get('/instagramUsers', 'RegisterInstagressController@instagramUsers');
Route::get('/instagramTags', 'RegisterInstagressController@instagramTags');
Route::post('/storeRelatedUsers', 'RegisterInstagressController@storeRelatedUsers');
Route::post('/storeRelatedTags', 'RegisterInstagressController@storeRelatedTags');
Route::post('/instagramFollow', 'RegisterInstagressController@storeFollow');
Route::post('/instagramCredentials', 'RegisterInstagressController@instagramCredentials');

Route::get('/dashboard', 'DashboardController@index');
Route::get('/dashboard/data', 'DashboardController@data');
Route::post('/dashboard/liking', 'DashboardController@liking');
Route::post('/dashboard/following', 'DashboardController@following');
Route::post('/dashboard/deleteRow', 'DashboardController@deleteRow');
Route::post('/dashboard/saveTargets', 'DashboardController@saveTargets');